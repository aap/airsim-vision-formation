import csv

import numpy as np

NUM_SAMPLES = 10000
X0, Y0 = -57.0, 84.0
Z = -60.0
R_MIN, R_MAX = 2.0, 20.0

# Reference: https://mathworld.wolfram.com/DiskPointPicking.html
r_arr = np.sqrt(np.random.uniform(R_MIN**2, R_MAX**2, NUM_SAMPLES))
ang_arr = np.random.uniform(0, 2*np.pi, NUM_SAMPLES)

x1_arr = X0 + r_arr * np.cos(ang_arr)
y1_arr = Y0 + r_arr * np.sin(ang_arr)

with open(f"sample_{NUM_SAMPLES}_poses-uniform_disk-[{R_MIN}-{R_MAX}].csv", 'w') as f:
    csv_writer = csv.DictWriter(f,
        fieldnames=[
            "X0", "Y0", "Z0", "QX0", "QY0", "QZ0", "QW0",
            "X1", "Y1", "Z1", "QX1", "QY1", "QZ1", "QW1"]
    )
    csv_writer.writeheader()
    csv_writer.writerows(
        rowdicts=({
            "X0": X0, "Y0": Y0, "Z0": Z,
            "QX0": 0, "QY0": 0, "QZ0": 0, "QW0": 1,
            "X1": x1, "Y1": y1, "Z1": Z,
            "QX1": 0, "QY1": 0, "QZ1": 0, "QW1": 1
        }
        for x1, y1 in zip(x1_arr, y1_arr))
    )
