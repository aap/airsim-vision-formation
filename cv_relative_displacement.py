"""
Estimate relative displacement between cameras with OpenCV

+ Feature detection using ORB
+ Feature matching using FLANN
+ Recover relative rotation with three approaches
  - `findHomography`, `decomposeHomographyMat`, and `triangulatePoints`
  - `findEssentialMat` and `recoverPose`
  - `findFundamentalMat` and `recoverPose`

See https://docs.opencv.org/3.4/dc/dc3/tutorial_py_matcher.html
for reference of the parameter values in feature matching.
"""

import pathlib
import pickle
from typing import Iterable, Literal, Sequence, Tuple
import warnings

import cv2
from matplotlib import pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation as R

np.set_printoptions(precision=3, suppress=True)

FLANN_IDX_PARAS = dict(
    algorithm = 6,  # FLANN_INDEX_LSH = 6
    table_number = 12,
    key_size = 20,
    multi_probe_level = 2)
FLANN_SEARCH_PARAS = dict(checks=50)  # or pass empty dictionary

# Check `jupyter/cv_to_ned.ipynb` for deriving this constant rotation matrix
R_CV2NED = R.from_matrix(np.asfarray([
    [0,-1, 0],
    [1, 0, 0],
    [0, 0, 1],
]))

UNIT_Z = np.array([0., 0., 1.]).T


def pixel_to_local_ned_frame(cam_mat: np.ndarray, pix_arr_T) -> np.ndarray:
    assert len(pix_arr_T.shape) == 2, f"Must be a 2D array. shape={pix_arr_T.shape}"
    assert pix_arr_T.shape[1] == 2, "Must be a Nx2 matrix."

    fx, fy, cx, cy = cam_mat[0, 0], cam_mat[1, 1], cam_mat[0, 2], cam_mat[1, 2]
    pt_arr_T = np.full(shape=(pix_arr_T.shape[0], 3), fill_value=np.nan)
    pt_arr_T[:, 0] = (pix_arr_T[:, 0] - cx) / fx
    pt_arr_T[:, 1] = (pix_arr_T[:, 1] - cy) / fy
    pt_arr_T[:, 2] = 1.0
    return R_CV2NED.apply(pt_arr_T)


def get_feature_points(img)-> Tuple:
    # Initiate ORB detector
    orb = cv2.ORB_create()
    # find the keypoints and compute the descriptors with ORB
    kp, des = orb.detectAndCompute(img, None)
    return kp, des


def feature_matching(des_i, des_j,
        num_max_matches: int = 50) -> Sequence[cv2.DMatch]:
    matcher = cv2.FlannBasedMatcher(FLANN_IDX_PARAS, FLANN_SEARCH_PARAS)
    matches = matcher.knnMatch(des_i, des_j, k=2)
    # Ensure match two
    matches = (match for match in matches if len(match) == 2)
    # Filter with ratio test as per Lowe's paper
    good_matches = [m for m, n in matches if m.distance < 0.7*n.distance]
    good_matches.sort(key=lambda x: x.distance)

    if len(good_matches) > num_max_matches:
        # Take the top `max_num_matches` after sorting
        return good_matches[:num_max_matches]
    else:
        return good_matches


def get_relative_rotation(
        pix_i_arr, pix_j_arr, cam_mat,
        method: Literal["HomographyMat", "FundamentalMat", "EssentialMat"] = "HomographyMat",
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    assert pix_i_arr.shape == pix_j_arr.shape
    if pix_i_arr.shape[0] < 8:
        raise ValueError("Less than 8 points for relative pose estimation")

    if method == "HomographyMat":
        hom_mat, mask = cv2.findHomography(pix_i_arr, pix_j_arr, method=cv2.LMEDS)
        ret_val, rot_ests, t_ests, normals = cv2.decomposeHomographyMat(hom_mat, cam_mat)

        fx, fy, cx, cy = cam_mat[0, 0], cam_mat[1, 1], cam_mat[0, 2], cam_mat[1, 2]

        # Convert from pixels to 2D points in camera frame 
        pt_i_arr = np.full(shape=pix_i_arr.shape, fill_value=np.nan)
        pt_j_arr = np.full(shape=pix_j_arr.shape, fill_value=np.nan)
        pt_i_arr[:, 0] = (pix_i_arr[:, 0] - cx) / fx
        pt_j_arr[:, 0] = (pix_j_arr[:, 0] - cx) / fx
        pt_i_arr[:, 1] = (pix_i_arr[:, 1] - cy) / fy
        pt_j_arr[:, 1] = (pix_j_arr[:, 1] - cy) / fy

        I_3x4 = np.eye(3, 4)
        masks = []
        for rot_k, t_k in zip(rot_ests, t_ests):
            proj_mat = np.column_stack((rot_k, t_k))
            tri_pts = cv2.triangulatePoints(I_3x4, proj_mat, pt_i_arr.T, pt_j_arr.T)
            mask = (tri_pts[2] * tri_pts[3] > 0)

            # Normalize homogeneous coordinates
            tri_pts[0] /= tri_pts[3]
            tri_pts[1] /= tri_pts[3]
            tri_pts[2] /= tri_pts[3]
            tri_pts[3] /= tri_pts[3]

            tri_pts = proj_mat @ tri_pts
            mask = (tri_pts[2] > 0) & mask  # Positive depth
            masks.append(mask)
       
        arg_max_idx = np.argmax([np.count_nonzero(mask) for mask in masks])
        mask = masks[arg_max_idx]
        rot_est = rot_ests[arg_max_idx]
    elif method == "FundamentalMat":
        fun_mat, mask = cv2.findFundamentalMat(pix_i_arr, pix_j_arr, method=cv2.FM_LMEDS)
        ess_mat = cam_mat.T @ fun_mat @ cam_mat
        ret_val, rot_est, t_est, mask = cv2.recoverPose(
            ess_mat, pix_i_arr, pix_j_arr, cameraMatrix=cam_mat, mask=mask)
    elif method == "EssentialMat":
        ess_mat, mask = cv2.findEssentialMat(
            pix_i_arr, pix_j_arr, cameraMatrix=cam_mat, method=cv2.LMEDS)
        ret_val, rot_est, t_est, mask = cv2.recoverPose(
            ess_mat, pix_i_arr, pix_j_arr, cameraMatrix=cam_mat, mask=mask)
    else:
        raise ValueError(f"Unknown method name '{method}'")

    # Return matched points and relative transform
    assert len(pix_i_arr[mask]) == len(pix_j_arr[mask])

    pt_i_arr_T = pixel_to_local_ned_frame(cam_mat, pix_i_arr[mask])
    pt_j_arr_T = pixel_to_local_ned_frame(cam_mat, pix_j_arr[mask])
    return pt_i_arr_T, pt_j_arr_T, rot_est


def get_relative_pos(altitude: float,
        src_rot: R, rel_rot: R, pt_src_arr_T: np.ndarray, pt_dst_arr_T: np.ndarray):
    vec_src_T = (src_rot.inv()).apply(pt_src_arr_T)
    s_src = np.tile(vec_src_T @ UNIT_Z, (3, 1)).T
    vec_dst_T = (src_rot.inv() * rel_rot.inv()).apply(pt_dst_arr_T)
    s_dst = np.tile(vec_dst_T @ UNIT_Z, (3, 1)).T
    return np.mean(-altitude*((vec_src_T) / s_src  - (vec_dst_T) / s_dst), axis=0)


def get_pairwise_matching_pixels(kp_i, kp_j, matches):
    pix_i_arr = np.asfarray([kp_i[m.queryIdx].pt for m in matches])
    pix_i_arr.shape = (-1, 2)
    pix_i_arr.flags.writeable = False
    pix_j_arr = np.asfarray([kp_j[m.trainIdx].pt for m in matches])
    pix_j_arr.shape = (-1, 2)
    pix_j_arr.flags.writeable = False
    return pix_i_arr, pix_j_arr


def generate_pairwise_relative_transform(
        img_seq: Sequence[np.ndarray],
        cam_mat: np.ndarray,
        adj_mat: np.ndarray
    ) -> Iterable[Tuple[Tuple[int, int], Tuple[np.ndarray, np.ndarray, np.ndarray]]]:
    """
    Generator producing estimated relative transformation for each pair of
    neighboring agents.
    """
    num_agents = len(img_seq)
    assert adj_mat.shape == (num_agents, num_agents)
    assert all(img.shape == img_seq[0].shape for img in img_seq)

    feat_pts = [get_feature_points(img) for img in img_seq]

    # List of nonrepeating pairs where 0 <= i < j < num_agents and i, j are neighbors
    pair_iter = ((i, j) for i in range(num_agents-1)
                 for j in range(i+1, num_agents) if adj_mat[i, j])

    num_max_matches = 50
    for i, j in pair_iter:
        pix_i_arr, pix_j_arr, rot_mat = np.empty(shape=(0, 2)), np.empty(shape=(0, 2)), None
        try:
            matches = feature_matching(feat_pts[i][1], feat_pts[j][1], num_max_matches)
            pix_i_arr, pix_j_arr = get_pairwise_matching_pixels(feat_pts[i][0], feat_pts[j][0], matches)
            pix_i_arr, pix_j_arr, rot_mat = get_relative_rotation(pix_i_arr, pix_j_arr, cam_mat)
        except (cv2.error, ValueError) as e:
            warnings.warn(str(e))

        yield (i, j), (pix_i_arr, pix_j_arr, rot_mat)


def plot_feature_detect_matching(img_i, img_j):
    # Initiate ORB detector
    orb = cv2.ORB_create()
    # find the keypoints and compute the descriptors with ORB
    kp_i, des_i = orb.detectAndCompute(img_i, None)
    kp_j, des_j = orb.detectAndCompute(img_j, None)

    # Take top at most 50 matches
    matches = feature_matching(des_i, des_j, 50)
    print(f"# matched features: {len(matches)}")

    pix_i_arr = np.asfarray([kp_i[m.queryIdx].pt for m in matches])
    pix_j_arr = np.asfarray([kp_j[m.trainIdx].pt for m in matches])

    # Draw matched feature points for debugging
    img3 = cv2.drawMatchesKnn(
        img_i, kp_i, img_j, kp_j,
        [[m] for m in matches], None,
        matchColor = (0, 255, 0),
        singlePointColor = (255, 0, 0),
        flags = cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS
    )
    plt.figure()
    plt.imshow(img3,)
    plt.show()


def test_feature_detect_matching():
    data_path = pathlib.Path("data/test_relative_pose_estimation")
    tiff_path_list = list(data_path.glob("*_drones.tiff"))

    for tiff_path in tiff_path_list:
        print("="*8, tiff_path.name)
        success, img_seq = cv2.imreadmulti(str(tiff_path))

        assert success, "Failed at loading the multi-page image"
        assert len(img_seq) >= 2
        # Only test feature matching with the first two images
        plot_feature_detect_matching(img_seq[0], img_seq[1])


def test_get_relative_displacement():
    IMG_W, IMG_H = 240, 240
    fy = fx = IMG_W // 2  # np.tan(90/2) == 1
    CAM_MAT = np.asfarray([
        [fx, 0, IMG_W//2],
        [0, fy, IMG_H//2],
        [0, 0, 1],
    ])
    ALT = -25
    UNIT_Z = np.array([0., 0., 1.]).T

    data_path = pathlib.Path("data/test_relative_pose_estimation")
    pkl_path_list = list(data_path.glob("*_true_pose.pickle"))
    pkl_path_list.sort()
    tiff_path_list = list(data_path.glob("*_drones.tiff"))
    tiff_path_list.sort()

    mag_list = []
    for pkl_path, tiff_path in zip(pkl_path_list, tiff_path_list):
        print("="*8, pkl_path.name, tiff_path.name)
        with open(pkl_path, 'rb') as f:
            pose_np_arr = pickle.load(f)

        success, img_seq = cv2.imreadmulti(str(tiff_path))
        assert success, "Failed at loading the multi-page image"
        for img in img_seq:
            assert (img.shape[1], img.shape[0]) == (IMG_W, IMG_H), \
                f"The expected size of images is {IMG_W}x{IMG_H}, " \
                f"but the size of given images is {img.shape[1]}x{img.shape[0]}."
        num_agents = len(img_seq)
        adj_mat = np.ones(shape=(num_agents, num_agents), dtype=bool) \
            & ~np.identity(num_agents, dtype=bool)  # Fully connected

        for (i, j), (pt_i_arr_T, pt_j_arr_T, rot_mat) in generate_pairwise_relative_transform(img_seq, CAM_MAT, adj_mat):
            pos_i, quat_i = pose_np_arr[i][0:3], pose_np_arr[i][3:7]
            pos_j, quat_j = pose_np_arr[j][0:3], pose_np_arr[j][3:7]
            # Rotation matrix used in pose estimation is the inverse.
            # i.e., convert from world to body frame instead of body to world frame.
            tru_rot_i = R.from_quat(quat_i).inv()
            tru_rot_j = R.from_quat(quat_j).inv()
            tru_rot_ij = tru_rot_j * tru_rot_i.inv()

            est_rot_ij = R.from_matrix(rot_mat)

            tru_z_ij = pos_j - pos_i

            vec_i_T = (tru_rot_i.inv()).apply(pt_i_arr_T)
            vec_j_T = (tru_rot_i.inv() * est_rot_ij.inv()).apply(pt_j_arr_T)
            s_i = np.tile(vec_i_T @ UNIT_Z, (3, 1)).T
            s_j = np.tile(vec_j_T @ UNIT_Z, (3, 1)).T
            est_z_ij = np.mean(-ALT*((vec_i_T) / s_i  - (vec_j_T) / s_j), axis=0)

            err_rot_mag = np.rad2deg((est_rot_ij * tru_rot_ij.inv()).magnitude())
            err_z_mag = np.linalg.norm(tru_z_ij - est_z_ij)
            mag_list.append((err_rot_mag, err_z_mag))
    if not mag_list:
        return
    min_mag, med_mag, avg_mag, max_mag = np.min(mag_list, axis=0), np.median(mag_list, axis=0), np.mean(mag_list, axis=0), np.max(mag_list, axis=0)
    print("<min, med, avg, max> error in R_ij: "
          "<%.3f, %.3f, %.3f, %.3f> deg" % (min_mag[0], med_mag[0], avg_mag[0], max_mag[0]))
    print("<min, med, avg, max> error in z_ij: "
          "<%.3f, %.3f, %.3f, %.3f> meter" % (min_mag[1], med_mag[1], avg_mag[1], max_mag[1]))


if __name__ == "__main__":
    test_feature_detect_matching()
    test_get_relative_displacement()
