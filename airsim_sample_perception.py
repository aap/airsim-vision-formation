#!/usr/bin/env python3

import argparse
import csv
from dataclasses import dataclass
from ipaddress import ip_address
import json
from pathlib import Path
from typing import Sequence, Tuple

import airsim
import numpy as np
import time


from airsim_scenario_runner import MotionAirSimDrone, WeatherKeyValueAction, get_logger_folder_path, get_perc_relative_position_as_complex_mat
from formation_config import topology_to_adjacency_matrix


THRES = 5*10**-3


@dataclass(frozen=True)
class SampleConfig:
    pose_pairs: Sequence[Tuple[airsim.Pose, airsim.Pose]]
    record_parent_path: Path
    settings_json_str: str


def build_sample_config(airsim_client: airsim.VehicleClient,
        args) -> SampleConfig:
    settings = json.loads(airsim_client.getSettingsString())
    rec_parent_path = Path(settings['Recording']['Folder'])

    reader = csv.DictReader(args.poses, delimiter=',')
    pose_pairs = []
    for raw_row in reader:
        row = {k: float(v) for k, v in raw_row.items()}
        pose0 = airsim.Pose(
            position_val=airsim.Vector3r(
                row["X0"], row["Y0"], row["Z0"]),
            orientation_val=airsim.Quaternionr(
                row["QX0"], row["QY0"], row["QZ0"], row["QW0"])
        )
        pose1 = airsim.Pose(
            position_val=airsim.Vector3r(
                row["X1"], row["Y1"], row["Z1"]),
            orientation_val=airsim.Quaternionr(
                row["QX1"], row["QY1"], row["QZ1"], row["QW1"])
        )
        pose_pairs.append((pose0, pose1))
    print(f"# pose pairs: {len(pose_pairs)}")

    weather_dict = settings.get("Weather", {})
    if args.weather is not None:
        weather_dict.update(args.weather)

    if weather_dict:
        settings['Weather'] = weather_dict

    # Set Weather Values
    if not weather_dict:
        airsim_client.simEnableWeather(False)
    else:
        print(weather_dict)
        airsim_client.simEnableWeather(True)
        for key, val in weather_dict.items():
            attr = getattr(airsim.WeatherParameter, key)
            airsim_client.simSetWeatherParameter(attr, val)

    return SampleConfig(
        pose_pairs=pose_pairs,
        record_parent_path=rec_parent_path,
        settings_json_str=json.dumps(settings, sort_keys=True)
    )


def set_scenes(
        airsim_client: airsim.VehicleClient,
        sample_config: SampleConfig,
        debug: bool = False) -> None:

    agents = [
        MotionAirSimDrone("UAV0", 0, airsim_client),
        MotionAirSimDrone("UAV1", 1, airsim_client)
    ]
    adj_mat = topology_to_adjacency_matrix(len(agents))

    takeoff_futures = []
    for agent in agents:
        agent.enable_drone()
        takeoff_futures.append(agent.takeoff_async())
    for f in takeoff_futures:
        f.join()

    time.sleep(1)

    camera_name = "BirdsEyeCamera"
    img_reqs = [airsim.ImageRequest(
        camera_name=camera_name,
        image_type=0
    )]

    print("Start recording!")
    airsim_client.startRecording()  # Recording files being created
    airsim_rec_folder_path = get_logger_folder_path(sample_config.record_parent_path)
    with open(airsim_rec_folder_path / "settings.json", "w") as f:
        f.write(sample_config.settings_json_str)
    print(f"`settings.json` is saved to {airsim_rec_folder_path} for reproducing sampling.\n")
    rec_folder = airsim_rec_folder_path
    print(f"Collected data is saved to {rec_folder}.")

    airsim_client.simPause(True)
    true_perc_pairs = []
    for i, (uav0_samp_pose, uav1_samp_pose) in enumerate(sample_config.pose_pairs):
        # Set drone 0 pose 
        airsim_client.simSetVehiclePose(
            vehicle_name=agents[0].drone_name,
            pose=airsim.Pose(
                position_val=agents[0]._position_w2h(uav0_samp_pose.position),
                orientation_val=uav1_samp_pose.orientation
            ),
            ignore_collision=True
        )
        # Set drone 1 pose 
        airsim_client.simSetVehiclePose(
            vehicle_name=agents[1].drone_name,
            pose=airsim.Pose(
                position_val=agents[1]._position_w2h(uav1_samp_pose.position),
                orientation_val=uav1_samp_pose.orientation
            ),
            ignore_collision=True
        )
        airsim_client.simContinueForFrames(2)

        uav0_pose_world = agents[0].pose_w
        uav1_pose_world = agents[1].pose_w

        if uav0_pose_world.position.distance_to(uav0_samp_pose.position) > THRES:
            print("something wrong when setting UAV0 to position. "
                  f"Expect:{uav0_samp_pose.position}. "
                  f"Actual:{uav0_pose_world.position}. "
                   "skip.")
            continue
        if uav1_pose_world.position.distance_to(uav1_samp_pose.position) > THRES:
            print(f"something wrong when setting UAV1 to position. "
                  f"Expect:{uav1_samp_pose.position}. "
                  f"Actual:{uav1_pose_world.position}. "
                   "skip.")
            continue
        # Compute estimated relative pos
        if debug:
            print("="*20 + str(i) + "="*20)
            rel_pos_mat = get_perc_relative_position_as_complex_mat(
                agents, adj_mat, uav0_pose_world.position.z_val, rec_folder=rec_folder, step=i)
        else:
            if i % 20 == 0:
                print(i, end='..', flush=True)
            rel_pos_mat = get_perc_relative_position_as_complex_mat(
                agents, adj_mat, uav0_pose_world.position.z_val, rec_folder=None, step=None)
        tru_rel_pos = uav1_pose_world.position.to_numpy_array() \
            - uav0_pose_world.position.to_numpy_array()
        tru_rel_pos = tru_rel_pos[0:2] @ np.array([1, 1.j])
        est_rel_pos = rel_pos_mat[0, 1]
        true_perc_pairs.append((tru_rel_pos, est_rel_pos))

        if not debug:
            continue

        # Save image
        img_resps: Sequence[airsim.ImageResponse] = \
            airsim_client.simGetImages(img_reqs, external=True)
        resp = img_resps[0]
        img_file_name = "_".join([
            "img", f"{i:04}", f"{camera_name}", f"{resp.image_type}", f"{resp.time_stamp}"
        ]) + ".png"
        airsim.write_file(rec_folder / "images" / img_file_name, resp.image_data_uint8)

    print("Stop recording!")
    agents[0]._airsim_client.stopRecording()
    print("# collected pairs:", len(true_perc_pairs))
    with open(rec_folder / "truth_percept_pair.npy", "wb") as npy_file:
        np.save(npy_file, np.array(true_perc_pairs, dtype=complex))


def main(args) -> None:
    airsim_client=airsim.MultirotorClient(ip=args.localhost_ip.exploded, port=args.port)
    airsim_client.confirmConnection()

    try:
        sample_config = build_sample_config(airsim_client, args)
        set_scenes(airsim_client, sample_config, debug=args.debug)
    except KeyboardInterrupt:
        print("User pressed Ctrl-C.")
    finally:
        print("Shutting down and reset vehicle states.")
        airsim_client = airsim.VehicleClient()
        airsim_client.confirmConnection()
        airsim_client.reset()
        airsim_client.simPause(False)
        airsim_client.simEnableWeather(True)
        airsim_client.simSetWeatherParameter(airsim.WeatherParameter.Fog, 0.0)
        airsim_client.simEnableWeather(False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    cmd_group = parser.add_argument_group('Command line only')
    cmd_group.add_argument(
        "poses", type=argparse.FileType('r'),
        help="CSV file specifying poses of drones for sampling images"
    )
    cmd_group.add_argument(
        "-i", "--localhost-ip", type=ip_address, dest="localhost_ip", default=ip_address("127.0.0.1"),
        help="IP address for connecting to the AirSim instance with matching `LocalhostIP`. "
             "(default: %(default)s)"
    )
    cmd_group.add_argument(
        "-p", "--port", type=int, default=41451,
        help="Port for connecting to the AirSim instance with matching `ApiServerPort`. "
             "(default: %(default)s)"
    )
    cmd_group.add_argument(
        "-W", "--weather", nargs='+', metavar="KEY=VALUE", action=WeatherKeyValueAction,
        help="Specify WeatherParameter values for AirSim. Values should be in [0, 1]."
    )
    cmd_group.add_argument(
        "--debug", action="store_true", default=False,
        help="Save bird's eye view images and bottom camera images for debugging."
    )
    main(parser.parse_args())
