#!/usr/bin/env python3

import argparse
from datetime import datetime, timedelta
from ipaddress import ip_address
import json
from pathlib import Path
import pickle
from timeit import default_timer as timer
from typing import Mapping, Sequence, Tuple

import airsim
import cv2
from msgpackrpc.future import Future
import numpy as np
from scipy.spatial.transform import Rotation as R
import time

from cv_relative_displacement import generate_pairwise_relative_transform, get_relative_pos
from formation_config import FormationConfig, get_form_sequence, topology_to_adjacency_matrix
# from velocity_control import PControlDefaultStop as PControl, SDPControl
from velocity_control import PControlSwitchTopology as PControl, SDPControl


class MotionAirSimDrone:
    BOT_CENTER_CAM = "bottom_center"
    R_CV2CAM = np.asfarray([
        [0, 0, 1],
        [1, 0, 0],
        [0, 1, 0],
    ])

    def __init__(self, drone_name: str, idx: int, airsim_client: airsim.MultirotorClient):
        assert idx >= 0
        self._drone_name = drone_name
        self._idx = idx
        self._airsim_client = airsim_client

        self._home_to_world_vec = (self.pose_w.position - self.pose.position)

    def get_cam_mat(self, img_width: int, img_height: int) -> np.ndarray:
        """ Extract camera intrinsic matrix from projection matrix in AirSim

        Check `jupyter/cv_to_ned.ipynb` for deriving camera intrinsic
              matrix from projection matrix in this application.
        """
        norm_xy = np.array([
            [-img_width/2, 0, img_width/2],
            [0, img_height/2, img_height/2],
            [0, 0, 1],
        ])

        cam_info = self._airsim_client.simGetCameraInfo(
            camera_name=self.BOT_CENTER_CAM,
            vehicle_name=self._drone_name
        )
        proj_mat_4x4 = np.asfarray(cam_info.proj_mat.matrix)

        # Drop row for Z-axis and the last column for translation vector
        proj_mat_3x3 = proj_mat_4x4[[0, 1, 3], :][:, 0:3]

        # Normalize and rotate
        cam_mat = -norm_xy @ proj_mat_3x3 @ self.R_CV2CAM
        assert np.all(cam_mat >= 0)
        return cam_mat

    def _position_h2w(self, position: airsim.Vector3r) -> airsim.Vector3r:
        return position + self._home_to_world_vec

    def _position_w2h(self, position_w: airsim.Vector3r) -> airsim.Vector3r:
        return position_w - self._home_to_world_vec

    @property
    def drone_name(self) -> str:
        return self._drone_name

    @property
    def idx(self) -> int:
        return self._idx

    def enable_drone(self) -> None:
        self._airsim_client.enableApiControl(is_enabled=True,
                                             vehicle_name=self.drone_name)
        self._airsim_client.armDisarm(arm=True, vehicle_name=self.drone_name)

    def reset_and_disable_drone(self) -> None:
        self._airsim_client.reset()
        self._airsim_client.armDisarm(arm=False, vehicle_name=self.drone_name)
        self._airsim_client.enableApiControl(is_enabled=False,
                                             vehicle_name=self.drone_name)

    def takeoff(self):
        f = self._airsim_client.takeoffAsync(vehicle_name=self.drone_name)
        f.join()

    def takeoff_async(self) -> "Future":
        return self._airsim_client.takeoffAsync(vehicle_name=self.drone_name)

    @property
    def pose_w(self) -> airsim.Pose:
        """Vehicle pose in the world NED frame."""
        return self._airsim_client.simGetObjectPose(self.drone_name)

    def move_to_position_w(self, position_w: airsim.Vector3r, velocity: float = 1.0) -> None:
        """Move vehicle to the position in the world NED frame."""
        position = self._position_w2h(position_w)
        f = self._airsim_client.moveToPositionAsync(
            *position, velocity=velocity,
            vehicle_name=self.drone_name)
        f.join()

    def move_by_velocity_z_w_async(self, vxy_w: airsim.Vector2r, z_w: float, duration: float) -> "Future":
        return self._airsim_client.moveByVelocityZAsync(
            vx=vxy_w.x_val,
            vy=vxy_w.y_val,
            z=z_w,
            duration=duration,
            vehicle_name=self.drone_name
        )

    @property
    def pose(self) -> airsim.Pose:
        """Vehicle pose in the NED frame of the vehicle's starting position.

        This is also called the *vehicle inertial frame* with the origin being the start of the vehicle.

        NOTE: The starting orientation of the vehicle is not considered in frame transformation.
        """
        return self._airsim_client.simGetVehiclePose(self.drone_name)

    def move_to_position_async(self, position: airsim.Vector3r, velocity: float = 1.0) -> "Future":
        """Move vehicle to the position in the NED frame of the vehicle's starting position."""
        return self._airsim_client.moveToPositionAsync(
            *position, velocity=velocity,
            vehicle_name=self.drone_name)

    def get_bottom_center_image(self) -> np.ndarray:
        response = self._airsim_client.simGetImage(camera_name=self.BOT_CENTER_CAM,
                                                   image_type=airsim.ImageType.Scene,
                                                   vehicle_name=self.drone_name)
        png = cv2.imdecode(airsim.string_to_uint8_array(response), cv2.IMREAD_UNCHANGED)
        return png

    def get_stamped_bottom_center_image(self) -> Tuple[int, np.ndarray]:
        request = airsim.ImageRequest(
            camera_name=self.BOT_CENTER_CAM,
            image_type=airsim.ImageType.Scene
        )
        responses = self._airsim_client.simGetImages(
            requests=[request],
            vehicle_name=self.drone_name)
        resp: airsim.ImageResponse = responses[0]
        png = cv2.imdecode(
            airsim.string_to_uint8_array(resp.image_data_uint8),
            cv2.IMREAD_UNCHANGED)
        return resp.time_stamp, png


def get_true_relative_position_as_complex_mat(
        agents: Sequence[MotionAirSimDrone], adj_mat: np.ndarray) -> np.ndarray:
    """ Calculate relative positions using ground truth """
    num_agents = len(agents)
    true_rel_pos = np.full(shape=(num_agents, num_agents), fill_value=(np.nan + np.nan*1j))
    for agt in agents:
        nbr_iter = (other for other in agents if adj_mat[agt.idx, other.idx])
        for nbr in nbr_iter:
            rel_airsim_pos = nbr.pose_w.position - agt.pose_w.position
            true_rel_pos[agt.idx, nbr.idx] = rel_airsim_pos.x_val + rel_airsim_pos.y_val*1j
    return true_rel_pos


def get_mock_relative_position_as_complex_mat(
        agents: Sequence[MotionAirSimDrone], adj_mat: np.ndarray,
        noise_bound: float = 0.5, distribution: str = "uniform_sphere") -> np.ndarray:
    """ Mock the vision-based perception with ground truth added with desired noises """
    num_agents = len(agents)
    true_rel_pos = get_true_relative_position_as_complex_mat(agents, adj_mat)
    if distribution == "uniform_sphere":
        ang_mat = np.random.uniform(low=0.0, high=2*np.pi, size=(num_agents, num_agents))
        noise_mat = noise_bound*(np.cos(ang_mat) + 1j*np.sin(ang_mat))
    elif distribution == "uniform_ball":
        ang_mat = np.random.uniform(low=0.0, high=2*np.pi, size=(num_agents, num_agents))
        r_mat = np.random.uniform(low=0.0, high=noise_bound, size=(num_agents, num_agents))
        noise_mat = r_mat * (np.cos(ang_mat) + 1j*np.sin(ang_mat))
    elif distribution == "uniform_rect":
        noise_x_mat = np.random.uniform(low=-noise_bound, high=noise_bound, size=(num_agents, num_agents))
        noise_y_mat = np.random.uniform(low=-noise_bound, high=noise_bound, size=(num_agents, num_agents))
        noise_mat = noise_x_mat + 1j*noise_y_mat
    else:
        raise ValueError(f"Unknown noise distribution parameter '{distribution}'")

    return true_rel_pos + noise_mat


def get_perc_relative_position_as_complex_mat(
        agents: Sequence[MotionAirSimDrone], adj_mat: np.ndarray, altitude: float,
        rec_folder: Path, step: int=None) -> np.ndarray:
    """ Estimate relative positions using vision-based perception """
    num_agents = len(agents)

    pose_arr = np.full(shape=(num_agents, 7), fill_value=np.nan)
    img_stamp_list = [None for _ in range(num_agents)]
    img_list = [None for _ in range(num_agents)]
    for agent in agents:
        pose_w = agent.pose_w
        img_stamp, img_rgb = agent.get_stamped_bottom_center_image()

        pose_arr[agent.idx][0:3] = pose_w.position.to_numpy_array()
        pose_arr[agent.idx][3:7] = pose_w.orientation.to_numpy_array()

        img_stamp_list[agent.idx] = img_stamp
        img_list[agent.idx] = img_rgb

    # NOTE: Assume all cameras are the same.
    img_0 = img_list[agents[0].idx]
    cam_mat = agents[0].get_cam_mat(img_width=img_0.shape[1], img_height=img_0.shape[0])

    rel_pos = np.full(shape=(num_agents, num_agents), fill_value=(np.nan + np.nan * 1j))
    for (i, j), (pt_i_arr_T, pt_j_arr_T, rot_mat) in generate_pairwise_relative_transform(img_list, cam_mat, adj_mat):
        if rot_mat is None:
            continue

        est_rot_ij = R.from_matrix(rot_mat)

        # Estimate j from i
        tru_rot_i = R.from_quat(pose_arr[i][3:7]).inv()
        est_z_ij = get_relative_pos(altitude, tru_rot_i, est_rot_ij, pt_i_arr_T, pt_j_arr_T)
        rel_pos[i, j] = est_z_ij[0] + est_z_ij[1] * 1j

        # Estimate i from j
        tru_rot_j = R.from_quat(pose_arr[j][3:7]).inv()
        est_rot_ji = est_rot_ij.inv()
        est_z_ji = get_relative_pos(altitude, tru_rot_j, est_rot_ji, pt_j_arr_T, pt_i_arr_T)
        rel_pos[j, i] = est_z_ji[0] + est_z_ji[1] * 1j

    if rec_folder is not None:
        min_stamp = min(img_stamp_list)
        if step is not None:
            true_pose_pkl = f'{step:04}_{min_stamp}_true_pose.pickle'
            drones_tiff = f'{step:04}_{min_stamp}_drones.tiff'
            perc_mat_pkl = f'{step:04}_{min_stamp}_perc_complex_mat.pickle'
        else:
            true_pose_pkl = f'{min_stamp}_true_pose.pickle'
            drones_tiff = f'{min_stamp}_drones.tiff'
            perc_mat_pkl = f'{min_stamp}_perc_complex_mat.pickle'
        with open(rec_folder / true_pose_pkl, 'ab') as f:
            pickle.dump(pose_arr, f)
        cv2.imwritemulti(str((rec_folder / drones_tiff).resolve()), img_list)
        with open(rec_folder / perc_mat_pkl, 'ab') as f:
            pickle.dump(rel_pos, f)
    return rel_pos


def get_logger_folder_path(record_parent_path: Path) -> Path:
    curr_sec = datetime.now()
    curr_sec_str = curr_sec.strftime("%Y-%m-%d-%H-%M-%S")
    record_folder_path = record_parent_path / curr_sec_str
    if record_folder_path.exists() and record_folder_path.is_dir():
        return record_folder_path

    # Probably created in previous second
    prev_sec = curr_sec - timedelta(seconds=1)
    prev_sec_str = prev_sec.strftime("%Y-%m-%d-%H-%M-%S")
    record_folder_path = record_parent_path / prev_sec_str
    if record_folder_path.exists() and record_folder_path.is_dir():
        return record_folder_path
    # else:
    raise RuntimeError("Cannot find recording folder from AirSim.")


def sim_sequential(
        airsim_client: airsim.MultirotorClient,
        agents: Sequence[MotionAirSimDrone],
        form_config: FormationConfig) -> None:
    takeoff_futures = []
    for agent in agents:
        agent.enable_drone()
        takeoff_futures.append(agent.takeoff_async())
    for f in takeoff_futures:
        f.join()

    # Fly drones to desired altitude
    altitude_futures = []
    for agent in agents:
        altitude_futures.append(
            agent.move_to_position_async(
                airsim.Vector3r(0, 0, form_config.altitude),
                velocity=10.0)
        )
    for f in altitude_futures:
        f.join()

    time.sleep(1)

    altitude = form_config.altitude
    form_seq = form_config.form_sequence
    time_horizon = form_config.time_horizon
    get_relative_pos = form_config.get_relative_pos
    vel_ctrl = form_config.velocity_control

    print("Start recording!")
    airsim_client.startRecording()  # Recording files being created
    airsim_rec_folder_path = get_logger_folder_path(form_config.record_parent_path)
    with open(airsim_rec_folder_path / "settings.json", "w") as f:
        f.write(form_config.settings_json_str)
    print(f"Merged `settings.json` is saved to {airsim_rec_folder_path} for reproducing the simulation.\n")
    set_form_stamp_path = airsim_rec_folder_path / "set_form_rec.txt"
    with open(set_form_stamp_path, "w") as f:
        f.write("TimeStamp\tFormIndex\n")
    print("Timestamps for formation changes are saved to `set_form_rec.txt`.")
    if not form_config.collect_data:
        rec_folder = None
    else:
        rec_folder = airsim_rec_folder_path
        print(f"Collected data is saved to {rec_folder}.")

    t_start = t_prev_start = timer()
    i = 0
    set_form_count = 0
    move_futures = []
    INTERVAL_TIME = 0.50
    while timer() < t_start + time_horizon:
        start_time = time.time()
        # Ensure setting new form at Iteration 0
        if set_form_count == 0 or \
                timer() >= t_start + set_form_count * form_config.form_interval:
            form_idx = set_form_count % len(form_seq)
            print("#"*80)
            print(f"Setting to Form #{form_idx}")
            vel_ctrl.set_new_form(form_seq[form_idx])
            set_form_count = set_form_count + 1
            # Recording timestamp when the desired form is changed
            curr_stamp = airsim_client.getMultirotorState().timestamp
            with open(set_form_stamp_path, 'a') as f:
                f.write(f"{curr_stamp}\t{form_idx}\n")
        print("="*80)
        t_curr_start = timer()
        print(f"Iteration {i} | Elapsed Time: {t_curr_start - t_start:.3f} | "
              f"Interval of Previous Iteration: {t_curr_start - t_prev_start:.3f}")
        t_prev_start = t_curr_start
        rel_pos = get_relative_pos(rec_folder)

        # Calculate linear velocity control
        v2d_arr = vel_ctrl.compute_velocity_ctrl(rel_pos)
        assert len(v2d_arr) == len(agents)

        # Wait for velocity commands from previous iteration to finish
        for f in move_futures:
            f.join()
        move_futures.clear()

        # Send new linear velocity control commands
        for agt in agents:
            v2d_ctrl = airsim.Vector2r(*v2d_arr[agt.idx])
            f = agt.move_by_velocity_z_w_async(v2d_ctrl, altitude, vel_ctrl.cmd_duration)
            move_futures.append(f)

        if time.time() - start_time < INTERVAL_TIME:
            time.sleep(INTERVAL_TIME - (time.time() - start_time))
        i = i + 1

    print("Stop recording!")
    airsim_client.stopRecording()


DEFAULT_RECORD_PARENT_FOLDER = str((Path.home() / "Documents" / "AirSim").resolve())


def merge_json_and_argparse(settings, args):
    form_config_json = settings["MultirotorFormation"]

    # Read all optional fields in json with default None or NaN
    record_parent_folder = settings.get("Recording", {}).get("Folder", DEFAULT_RECORD_PARENT_FOLDER)
    collect_data = form_config_json.get("CollectData", None)
    form_interval = float(form_config_json.get("FormInterval", np.nan))
    time_horizon = float(form_config_json.get("TimeHorizon", np.nan))
    altitude = float(form_config_json.get("Altitude", np.nan))
    perc = form_config_json.get("Perception", None)

    velocity_control_json = form_config_json.get("VelocityControl", None)
    if velocity_control_json is not None:
        strategy = velocity_control_json.get("Strategy", None)
        max_velocity = float(velocity_control_json.get("MaxVelocity", np.nan))
        cmd_duration = float(velocity_control_json.get("CommandDuration", np.nan))
    else:
        strategy = None
        max_velocity = np.nan
        cmd_duration = np.nan

    weather_dict = settings.get("Weather", {})

    # Override json value with command line value
    collect_data = bool(collect_data or args.collect_data)

    # Update weather values
    if args.weather is not None:
        weather_dict.update(args.weather)
    if args.form_interval is not None:
        form_interval = args.form_interval
    if args.time_horizon is not None:
        time_horizon = args.time_horizon
    if args.altitude is not None:
        altitude = args.altitude
    if args.perc is not None:
        perc = args.perc
    if args.strategy is not None:
        strategy = args.strategy
    if args.max_velocity is not None:
        max_velocity = args.max_velocity
    if args.cmd_duration is not None:
        cmd_duration = args.cmd_duration

    # Double check, normalize, and set default values
    if np.isnan(form_interval):
        form_interval = 15.0
    if np.isnan(time_horizon):
        time_horizon = 30.0
    if np.isnan(altitude):
        altitude = -25.0
    elif altitude >= 0:
        raise ValueError(f"Altitude '{altitude}'should be negative.")

    if not perc:
        perc = "GroundTruth"
    elif perc in ["gt", "GroundTruth"]:
        perc = "GroundTruth"
    elif perc in ["vis", "Vision"]:
        perc = "Vision"
    elif perc in ["mock", "Mock"]:
        perc = "Mock"
    elif not isinstance(perc, str):
        raise TypeError(f"Value for Perception '{perc}' is not a string.")
    else:
        raise ValueError(f"Unknown perception '{perc}'")
    
    if not strategy:
        strategy = "PControl"
    elif strategy in ["p_ctrl", "PControl"]:
        strategy = "PControl"
    elif strategy in ["sdp", "SDPControl"]:
        strategy = "SDPControl"
    elif not isinstance(strategy, str):
        raise TypeError(f"Value for VelocityControl.Strategy '{strategy}' is not a string.")
    else:
        raise ValueError(f"Unknown control strategy '{strategy}'")
    
    if np.isnan(max_velocity):
        max_velocity = 1.0
    if np.isnan(cmd_duration):
        cmd_duration = 0.2

    # Write back to json for sanity check
    settings["Recording"]["Folder"] = record_parent_folder
    settings["MultirotorFormation"]["CollectData"] = collect_data
    settings["MultirotorFormation"]["FormInterval"] = form_interval
    settings["MultirotorFormation"]["TimeHorizon"] = time_horizon
    settings["MultirotorFormation"]["Altitude"] = altitude
    settings["MultirotorFormation"]["Perception"] = perc
    settings["MultirotorFormation"]["VelocityControl"]["Strategy"] = strategy
    settings["MultirotorFormation"]["VelocityControl"]["MaxVelocity"] = max_velocity
    settings["MultirotorFormation"]["VelocityControl"]["CommandDuration"] = cmd_duration
    if weather_dict:
        settings['Weather'] = weather_dict
    return settings


def build_drone_agents_and_formation(
    airsim_client: airsim.MultirotorClient, args) \
        -> Tuple[Sequence[MotionAirSimDrone], FormationConfig]:
    merged_settings = merge_json_and_argparse(
        json.loads(airsim_client.getSettingsString()),
        args
    )

    # Set Weather Values
    weather_dict: Mapping = merged_settings.get('Weather')
    if not weather_dict:
        airsim_client.simEnableWeather(False)
    else:
        print(weather_dict)
        airsim_client.simEnableWeather(True)
        for key, val in weather_dict.items():
            attr = getattr(airsim.WeatherParameter, key)
            airsim_client.simSetWeatherParameter(attr, val)

    vehicle_dict = merged_settings["Vehicles"]

    # Get only vehicles with type SimpleFlight
    name_list = [
        name for name in airsim_client.listVehicles()
        if vehicle_dict[name]["VehicleType"] == "SimpleFlight"
    ]
    name_list.sort()  # Ensure order of the names
    agents = [MotionAirSimDrone(name, idx, airsim_client) for idx, name in enumerate(name_list)]

    rec_parent_path = Path(merged_settings['Recording']['Folder'])

    # Read fields in json for formation control
    form_config_json = merged_settings["MultirotorFormation"]
    collect_data = form_config_json["CollectData"]
    form_interval = form_config_json["FormInterval"] 
    time_horizon = form_config_json["TimeHorizon"]
    altitude = form_config_json["Altitude"]
    perc = form_config_json["Perception"]
    strategy = form_config_json["VelocityControl"]["Strategy"]
    max_velocity = form_config_json["VelocityControl"]["MaxVelocity"]
    cmd_duration = form_config_json["VelocityControl"]["CommandDuration"]
    form_sequence = get_form_sequence(
        name_seq=name_list,
        form_dict_seq=form_config_json["FormSequence"]
    )

    # Build the configuration object from bottom up
    num_agents = len(agents)
    # TODO: Support specifying adjacency matrix or other for communication graphs
    adj_mat = topology_to_adjacency_matrix(num_agents)

    if perc == "Vision":
        def get_relative_pos(rec_folder: Path):
            return get_perc_relative_position_as_complex_mat(
                agents, adj_mat, form_config.altitude, rec_folder
            )
    elif perc == "Mock":
        raise NotImplementedError("Current implementation of mocked perception is not finished.")
        # TODO
        def get_relative_pos(rec_folder: Path):
            return get_mock_relative_position_as_complex_mat(agents, adj_mat)
    else:
        assert perc == "GroundTruth"
        def get_relative_pos(rec_folder: Path):
            return get_true_relative_position_as_complex_mat(agents, adj_mat)

    if strategy == "PControl":
        velocity_control = PControl(num_agents, adj_mat, max_velocity, cmd_duration)
    else:
        assert strategy  == "SDPControl"
        velocity_control = SDPControl(num_agents, adj_mat, max_velocity, cmd_duration)

    settings_json_str = json.dumps(merged_settings, sort_keys=True)

    form_config = FormationConfig(
        adjacency_matrix=adj_mat,
        collect_data=collect_data,
        altitude=altitude,
        form_interval=form_interval,
        form_sequence=form_sequence,
        time_horizon=time_horizon,
        record_parent_path=rec_parent_path,
        get_relative_pos=get_relative_pos,
        velocity_control=velocity_control,
        settings_json_str=settings_json_str
    )
    return agents, form_config



def main(args) -> None:
    airsim_client = airsim.MultirotorClient(ip=args.localhost_ip.exploded, port=args.port)
    airsim_client.confirmConnection()

    agents, form_config = build_drone_agents_and_formation(airsim_client, args)

    try:
        if args.mode == "sequential":
            sim_sequential(airsim_client=airsim_client,
                           agents=agents,
                           form_config=form_config)
        else:
            assert args.mode == "concurrent"
            raise NotImplementedError("Concurrent simulation is not supported yet")
    except KeyboardInterrupt:
        print("User pressed Ctrl-C.")
    finally:
        print("Shutting down and reset vehicle states.")
        airsim_client = airsim.VehicleClient(ip=args.localhost_ip.exploded, port=args.port)
        airsim_client.stopRecording()
        airsim_client.reset()
        airsim_client.simEnableWeather(False)


class WeatherKeyValueAction(argparse.Action):
    KEYS = [
        "Rain",
        "Roadwetness",
        "Snow",
        "RoadSnow",
        "MapleLeaf",
        "RoadLeaf",
        "Dust",
        "Fog"
    ]
    def __call__(self, parser, namespace,
                 values, option_string = None):
        setattr(namespace, self.dest, dict())
          
        for value in values:
            # split it into key and value
            key, val_str = value.split('=')
            if  key not in self.KEYS:
                raise ValueError(f"Unknown WeatherParameter '{key}'.")
            val = float(val_str)
            if not (0.0 <= val <= 1.0):
                raise ValueError(f"Value '{val}' for WeatherParameter '{key}' is not within [0, 1].")
            # assign into dictionary
            getattr(namespace, self.dest)[key] = val


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    cmd_group = parser.add_argument_group('Command line only')
    cmd_group.add_argument(
        "-m", "--mode", choices=["sequential", "concurrent"], default="sequential",
        help="Simulation with all drones controlled by one sequential program (sequential), "
             "or each drone controlled by one program (concurrent).")
    cmd_group.add_argument(
        "-i", "--localhost-ip", type=ip_address, dest="localhost_ip", default=ip_address("127.0.0.1"),
        help="IP address for connecting to the AirSim instance with matching `LocalhostIP`. "
             "(default: %(default)s)"
    )
    cmd_group.add_argument(
        "-p", "--port", type=int, default=41451,
        help="Port for connecting to the AirSim instance with matching `ApiServerPort`. "
             "(default: %(default)s)"
    )

    # Arguments overriding JSON configurations
    json_group = parser.add_argument_group('Override JSON')
    json_group.add_argument(
        "-c", "-C", "--collect-data", action="store_true", dest="collect_data",
        help="Enable collecting camera images and vehicle poses. "
             "This saves to the same folder as in setting `Recording.Folder` for AirSim."
    )
    json_group.add_argument(
        "-I", "--form-interval", type=float, dest="form_interval",
        help="Override `FormInterval` specified for `MultirotorFormation`."
    )
    json_group.add_argument(
        "-T", "--time-horizon", type=float, dest="time_horizon",
        help="Override `TimeHorizon` specified for `MultirotorFormation`."
    )
    json_group.add_argument(
        "-Z", "--altitude", type=float, dest="altitude",
        help="Override `Altitude` specified for `MultirotorFormation`."
    )
    json_group.add_argument(
        "-P", "--perc", choices=["gt", "GroundTruth", "mock", "Mock", "vis", "Vision"], type=str,
        help="Override `Perception` specified for `MultirotorFormation`. "
             "gt or GroundTruth: ground truth; mock or Mock: mocked; "
             "vis or Vision: vision-based.")
    json_group.add_argument(
        "-S", "--strategy", choices=["p_ctrl", "PControl", "sdp", "SDPControl"], type=str,
        help="Override `Strategy` specified for `MultirotorFormation.VelocityControl`. "
             "p_ctrl or PControl: Proportional control; "
             "sdp or SDPControl: Semi-definite Programming.")
    json_group.add_argument(
        "-D", "--command-duration", type=float, dest="cmd_duration",
        help="Override `CommandDuration` specified for `MultirotorFormation.VelocityControl`."
    )
    json_group.add_argument(
        "-V", "--max_velocity", type=float, dest="max_velocity",
        help="Override `MaxVelocity` specified for `MultirotorFormation.VelocityControl`."
    )

    json_group.add_argument(
        "-W", "--weather", nargs='+', metavar="KEY=VALUE", action=WeatherKeyValueAction,
        help="Specify WeatherParameter values for AirSim. Values should be in [0, 1]."
    )
    main(parser.parse_args())
