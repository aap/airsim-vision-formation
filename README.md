# Vision-based formation control in AirSim environment


## Installation

```bash
$ pip3 install -r requirements.txt
```

Note that `pip` may throw an exception `ModuleNotFoundError: No module named 'msgpackrpc'` when installing `airsim`.
In this case, manually install `msgpack-rpc-python` first, then install all other requirements in `requirements.txt`.
That is,
```bash
$ pip3 install msgpack-rpc-python
$ pip3 install -r requirements.txt
```


## Run simulation

Download binary releases of AirSim environments from https://github.com/Microsoft/AirSim/releases.
We tested with the assets under **AirSim v1.8.1 - Linux** release.

Here we use **LandscapeMountains** for Linux as an example and extracts it under the project folder.
The path to the executable script should be `./LandscapeMountains/LinuxNoEditor/LandscapeMountains.sh`.
Under Linux environment, run the following command. You should see three quadrotors spawned in a mountain landscape.
```bash
$ ./LandscapeMountains/LinuxNoEditor/LandscapeMountains.sh -windowed -settings=$(pwd)/settings.LandscapeMountains.3drones.json
```


**Trouble Shooting:**

If there is only one car or quadrotor shown in the simulation environment,
the local JSON setting file `settings.LandscapeMountains.3drones.json` is not loaded by AirSim properly.
You may try the following command line argument instead.
```bash
$ ./LandscapeMountains/LinuxNoEditor/LandscapeMountains.sh -windowed --settings $(pwd)/settings.LandscapeMountains.3drones.json
```

Alternatively, you can move `settings.LandscapeMountains.3drones.json` to the default location `$HOME/Documents/AirSim`,
rename it as the default name `settings.json`, and run the command.
That is,
```bash
$ cp settings.LandscapeMountains.3drones.json $HOME/Documents/AirSim/settings.json
$ ./LandscapeMountains/LinuxNoEditor/LandscapeMountains.sh -windowed
```

### Simulate formation with default settings

Open another terminal and run the command below.
```bash
$ ./airsim_scenario_runner.py
```
This command simulates the formation control with the settings specified in
`settings.LandscapeMountains.3drones.json`.


### Simulate formation with ground truth perception

This command simulates the formation control with ground truth relative positions between quadrotors.
**After** all quadrotors have finished takeoff, the script will simulate for 20 seconds,
reset the states of all quadrotors, and terminate.
```bash
$ ./airsim_scenario_runner.py --perc gt -T 20
```


### Simulate formation with vision and collect data

We can use the option `--perc vis` to run the formation control simulation with vision and the flag `-C` to collect data.
```bash
$ ./airsim_scenario_runner.py --perc vis -T 20 -C
```

The collected data will be stored in the recording directory created by AirSim.
The collected data are the collected images `{timestamp}_drones.tiff`, the true poses `{timestamp}_true_pose.pickle`, and the estimated relative positions `{timestamp}_perc_complex_mat.pickle`.
Each tiff file contains N images from all N quadrotors at a synchronous step.
Each pickle file for true poses stores a N×7 NumPy array of true poses in the world coordinate.
Each row concatenates the 3D position and 4D quaternion obtained from the AirSim API `to_numpy_array`.
Each pickle file for estimated relative positions stores a N×N NumPy **complex** matrix of pairwise 2D relative positions (as complex numbers) in the world coordinate.


**Warning:** Currently, time intervals between consecutive synchronous steps can vary significantly due to the fluctuating computation time of vision algorithms.


## References

OpenCV: Camera Calibration and 3D Reconstruction
https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html

OpenCV: Epipolar Geometry
https://docs.opencv.org/4.5.5/da/de9/tutorial_py_epipolar_geometry.html

K. Oh, M. Park, H. Ahn,
"A survey of multi-agent formation control,"
in *J. Automatica*, vol. 53, pp. 424-440, 2015,
doi: [10.1016/j.automatica.2014.10.022](https://doi.org/10.1016/j.automatica.2014.10.022)

Dimos V. Dimarogonas, Kostas J. Kyriakopoulos,
"A connection between formation infeasibility and velocity alignment in kinematic multi-agent systems,"
in *J. Automatica*, vol. 44, no. 10, pp. 2648-2654, 2008,
doi: [10.1016/j.automatica.2008.03.013](https://doi.org/10.1016/j.automatica.2008.03.013)

K. Fathian, J. P. Ramirez-Paredes, E. A. Doucette, J. W. Curtis and N. R. Gans,
"QuEst: A Quaternion-Based Approach for Camera Motion Estimation From Minimal Feature Points,"
in *IEEE Robotics and Automation Letters*, vol. 3, no. 2, pp. 857-864, April 2018,
doi: [10.1109/LRA.2018.2792142](https://doi.org/10.1109/LRA.2018.2792142).

K. Fathian, E. Doucette, J. W. Curtis and N. R. Gans,
"Vision-Based Distributed Formation Control of Unmanned Aerial Vehicles,"
2018, *arXiv:1809.00096*,
doi: [10.48550/arXiv.1809.00096](https://doi.org/10.48550/arXiv.1809.00096).
