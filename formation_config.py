from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Mapping, Sequence

from airsim import Pose, Vector3r
from airsim.utils import to_quaternion
import numpy as np

from velocity_control import VelocityControlBase


@dataclass(frozen=True)
class FormationConfig:
    adjacency_matrix: np.ndarray  # NxN array
    collect_data: bool
    altitude: float
    form_interval: float
    form_sequence: Sequence[np.ndarray]  # A sequence of Nx2 arrays
    time_horizon: float
    record_parent_path: Path
    get_relative_pos: Callable
    velocity_control: VelocityControlBase
    settings_json_str: str


def topology_to_adjacency_matrix(num_agents: int) -> np.ndarray:
    # TODO different topologies
    adj_mat = np.ones(shape=(num_agents, num_agents), dtype=bool) \
        & ~np.identity(num_agents, dtype=bool)  # Fully connected
    adj_mat.flags.writeable = False
    return adj_mat


def get_simple_flight_settings(airsim_settings_vehicles):
    return {
        k: v for k, v in airsim_settings_vehicles.items()
        if v["VehicleType"] == "SimpleFlight"
    }


def get_init_pose_dict(airsim_settings_vehicles) -> Mapping[str, Pose]:
    assert all(v["VehicleType"] == "SimpleFlight"
               for v in airsim_settings_vehicles.values())
    def convert_dict_to_pose(pose_dict: Mapping[str, float]):
        return Pose(
            position_val=Vector3r(
                x_val=pose_dict.get('X', 0.0),
                y_val=pose_dict.get('Y', 0.0),
                z_val=pose_dict.get('Z', 0.0)),
            orientation_val=to_quaternion(
                roll=pose_dict.get('Roll', 0.0),
                pitch=pose_dict.get('Pitch', 0.0),
                yaw=pose_dict.get('Yaw', 0.0))
        )
    return {key: convert_dict_to_pose(val)
        for key, val in airsim_settings_vehicles.items()}


def get_form_sequence(name_seq: Sequence[str], form_dict_seq) \
        -> Sequence[np.ndarray]:
    form_list = []
    for form_dict in form_dict_seq:
        form = np.full(shape=(len(name_seq), 2), fill_value=np.nan)
        for i, name in enumerate(name_seq):
            dict_pos2D = form_dict[name]
            form[i][0] = float(dict_pos2D['X'])
            form[i][1] = float(dict_pos2D['Y'])
        form.flags.writeable = False
        form_list.append(form)
    return tuple(form_list)
