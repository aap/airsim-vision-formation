import pyvista as pv
import numpy as np
from scipy.spatial.transform import Rotation as R
import polytope as pc
import copy

# Desired altitude
Z = 20

# Define default camera intrinsic
IMG_W, IMG_H = 240, 240  # pixel
FoV = 90  # degree
fy = fx = IMG_W / (2*np.tan(np.deg2rad(FoV/2)))  # meter

K_i = np.asfarray([
    [fx, 0, IMG_W / 2],
    [0, fy, IMG_H / 2],
    [0, 0, 1],
])
K_j = K_i

# Define two camera frames using rotation matrices and translation vectors
O_w = np.zeros(3)
O_i = np.asfarray([0, 0, -Z])
O_j = np.asfarray([4, 0, -Z])

# Coordinate transformation from camera to world frame
R_i_C2W, t_i_C2W = R.from_euler(
    seq="XYZ", angles=(10, 0, 0), degrees=True), O_i - O_w
R_j_C2W, t_j_C2W = R.from_euler(
    seq="XYZ", angles=(0, 0, 0), degrees=True), O_j - O_w

# Derive the viewing regions of the two drones from the corner of the image plane
p_ex_T = np.array([
    [0, 0, 1],
    [IMG_W, 0, 1],
    [IMG_W, IMG_H, 1],
    [0, IMG_H, 1],
    [0, 0, 1],
])

# Get the range of extreme points for drone i
tmp_ex_i_T = R_i_C2W.apply((np.linalg.inv(K_i)@(p_ex_T).T).T)
X_ex_i_T = Z*tmp_ex_i_T/np.tile(tmp_ex_i_T[:, -1], (3, 1)).T+t_i_C2W
print(X_ex_i_T)

# Get the range of extreme points for drone j
tmp_ex_j_T = R_j_C2W.apply((np.linalg.inv(K_j)@(p_ex_T).T).T)
X_ex_j_T = Z*tmp_ex_j_T/np.tile(tmp_ex_j_T[:, -1], (3, 1)).T+t_j_C2W
print(X_ex_j_T)

# Compute the intersection between the two viewing regions from the two drones
poly_i = pc.qhull(vertices=X_ex_i_T[:, :2])
print(pc.extreme(poly_i))

poly_j = pc.qhull(vertices=X_ex_j_T[:, :2])
print(pc.extreme(poly_j))

poly_ij_intersect = pc.intersect(poly_i, poly_j)
print(poly_ij_intersect)

common_extremes = pc.extreme(poly_ij_intersect)
print(common_extremes)


# Perform 3D visualization


def plot3d(node, x_dim, y_dim, z_dim, ax):
    lower_bound = []
    upper_bound = []
    for key in sorted(node.lower_bound):
        lower_bound.append(node.lower_bound[key])
    for key in sorted(node.upper_bound):
        upper_bound.append(node.upper_bound[key])

    for i in range(min(len(lower_bound), len(upper_bound))):
        lb = list(map(float, lower_bound[i]))
        ub = list(map(float, upper_bound[i]))

        box = [[lb[x_dim], lb[y_dim], lb[z_dim]],
               [ub[x_dim], ub[y_dim], ub[z_dim]]]
        poly = pc.box2poly(np.array(box).T)
        plot_polytope_3d(poly.A, poly.b, ax=ax, color='#b3de69')


def plot_polytope_3d(A, b, ax, color='red', trans=0.2, edge=True):
    poly = pc.Polytope(A=A, b=b)
    vertices = pc.extreme(poly)
    cloud = pv.PolyData(vertices)
    volume = cloud.delaunay_3d()
    shell = volume.extract_geometry()
    ax.add_mesh(shell, opacity=trans, color=color)
    if edge:
        edges = shell.extract_feature_edges(20)
        ax.add_mesh(edges, color="k", line_width=1)


def plot_line_3d(start, end, ax, color='blue', line_width=1):
    a = start
    b = end

    # Preview how this line intersects this mesh
    line = pv.Line(a, b)
    ax.add_mesh(line, color=color, line_width=line_width)


def plot_dashline_3d(start, end, ax, color='blue', line_width=1, seg=0.1):
    num = int(np.linalg.norm(end-start)/seg)

    point_list = np.linspace(start, end, num)

    for i in range(0, num-1, 2):
        plot_line_3d(
            point_list[i, :], point_list[i+1, :], ax=ax, color=color, line_width=line_width)


def plot_point_3d(points, ax, color='blue', point_size=100):
    ax.add_points(points, render_points_as_spheres=True,
                  point_size=point_size, color=color)


ax = pv.Plotter()
plot_point_3d(O_i, ax=ax, color='blue', point_size=20)
projected_O_i = copy.deepcopy(O_i)
projected_O_i[2] = 0
plot_point_3d(projected_O_i, ax=ax, color='black', point_size=10)
plot_dashline_3d(O_i, projected_O_i, ax,
                 color='black', line_width=3, seg=0.3)
projected_camera_i = Z / \
    np.dot(R_i_C2W.apply([0, 0, 1]), [0, 0, 1])*R_i_C2W.apply([0, 0, 1]) + O_i
plot_point_3d(projected_camera_i, ax=ax, color='black', point_size=10)
plot_line_3d(O_i, projected_camera_i, ax, color='blue', line_width=3)
plot_dashline_3d(projected_O_i, projected_camera_i,
                 ax, color='black', line_width=3, seg=0.3)

plot_point_3d(O_j, ax, color='green', point_size=20)
projected_O_j = copy.deepcopy(O_j)
projected_O_j[2] = 0
plot_point_3d(projected_O_j, ax=ax, color='black', point_size=10)
plot_dashline_3d(O_j, projected_O_j, ax,
                 color='black', line_width=3, seg=0.3)
projected_camera_j = Z / \
    np.dot(R_j_C2W.apply([0, 0, 1]), [0, 0, 1])*R_j_C2W.apply([0, 0, 1]) + O_j
plot_point_3d(projected_camera_j, ax=ax, color='black', point_size=10)
plot_line_3d(O_j, projected_camera_j, ax, color='green', line_width=3)
plot_dashline_3d(projected_O_j, projected_camera_j,
                 ax, color='black', line_width=3, seg=0.3)

plot_point_3d(X_ex_i_T, ax, color='blue', point_size=10)
plot_point_3d(X_ex_j_T, ax, color='green', point_size=10)
box = np.array([[-1000, -1000, 0], [1000, 1000, 10]]).T
poly = pc.box2poly(box)
plot_polytope_3d(poly.A, poly.b, ax, color='white')
for i in range(X_ex_i_T.shape[0]-1):
    plot_line_3d(X_ex_i_T[i, :], X_ex_i_T[i+1, :],
                 ax=ax, color='blue', line_width=3)
    plot_line_3d(X_ex_i_T[i, :], O_i, ax=ax, color='blue', line_width=3)
for i in range(X_ex_j_T.shape[0]-1):
    plot_line_3d(X_ex_j_T[i, :], X_ex_j_T[i+1, :],
                 ax=ax, color='green', line_width=3)
    plot_line_3d(X_ex_j_T[i, :], O_j, ax=ax, color='green', line_width=3)
tmp = np.concatenate((common_extremes, np.zeros(
    (common_extremes.shape[0], 1))), axis=1)
for i in range(tmp.shape[0]):
    plot_line_3d(tmp[i, :], tmp[(i+1) % tmp.shape[0], :],
                 ax=ax, color='red', line_width=10)

# TODO set a good world camera view for better visualization
ax.show()
