#!/usr/bin/env python3

import argparse
import itertools
import json
from math import comb
from pathlib import Path
from typing import Dict, Tuple

from matplotlib.animation import FuncAnimation
from matplotlib.collections import LineCollection
import matplotlib.pyplot as plt
import numpy as np

from analyze_sim_traces import SETTINGS_JSON, T_KEY, align_with_airsim_rec, get_one_simulation_run
from formation_config import get_form_sequence, get_init_pose_dict, get_simple_flight_settings


def complex_to_pos2d(xy: complex) -> Tuple[float, float]:
    return np.real(xy), np.imag(xy)


def animate_one_trace_in_world_frame(
        stamp_arr: np.ndarray,
        vehicle_trace_dict: Dict[str, np.ndarray],
        desired_trace: np.ndarray,
        vehicle_cmap
    ):
    # Align the 0th vehicle always as the origin
    name_0 = sorted(list(vehicle_trace_dict.keys()))[0]
    dst_pos_arr_0 = np.copy(desired_trace[name_0])
    trace_0 = vehicle_trace_dict[name_0]
    for name in vehicle_trace_dict.keys():
        desired_trace[name] = desired_trace[name] - dst_pos_arr_0 \
             + trace_0['X'] + trace_0['Y']*1.j

    fig_pos = plt.figure("Position Trace")
    fig_pos.set_tight_layout(True)
    fig_pos.gca().set_aspect("equal")

    mins, maxes = [], []
    for trace in vehicle_trace_dict.values():
        mins.append((np.nanmin(trace['X']), np.nanmin(trace['Y'])))
        maxes.append((np.nanmax(trace['X']), np.nanmax(trace['Y'])))
    x_min, y_min = np.min(mins, axis=0)
    x_max, y_max = np.max(maxes, axis=0)
    fig_pos.gca().set_xlim(x_min-1, x_max+1)
    fig_pos.gca().set_ylim(y_min-1, y_max+1)

    line_col = LineCollection([], edgecolors='g', linestyles=':')
    fig_pos.gca().add_collection(line_col)
    iter_artists = [line_col]
    for name in vehicle_trace_dict.keys():
        plot, = fig_pos.gca().plot([], [], 'x', markersize=15, c=vehicle_cmap[name])
        iter_artists.append(plot)

    def animate(i):
        pos_dst = [complex_to_pos2d(desired_trace[name][i])
                   for name in vehicle_trace_dict.keys()]
        # Plot line segment between each pair of vehicles
        line_segs = list(itertools.combinations(pos_dst, 2))
        iter_artists[0].set_segments(line_segs)
        for plot, trace in zip(iter_artists[1:], vehicle_trace_dict.values()):
            plot.set_data(trace['X'][i], trace['Y'][i])
        return iter_artists

    ani = FuncAnimation(fig_pos, func=animate, frames=range(len(stamp_arr)), interval=10, blit=True, repeat=False)
    plt.show()


def animate_one_trace_relative_dist(
        stamp_arr: np.ndarray,
        vehicle_trace_dict: Dict[str, np.ndarray],
        desired_trace: np.ndarray,
        pw_const_bound = None
    ):
    num_agents = len(vehicle_trace_dict)
    assert num_agents > 2, "Need more than two agents"
    fig_dist, ax = plt.subplots(
        num="Relative Distance", nrows=comb(num_agents, 2),
        sharex=True)
    fig_dist.set_tight_layout(True)
    plt.setp(ax, xlim=(stamp_arr[0], stamp_arr[-1]))

    pair_list = [((name_i, trace_i), (name_j, trace_j))
                 for name_i, trace_i in vehicle_trace_dict.items()
                 for name_j, trace_j in vehicle_trace_dict.items()
                 if name_i < name_j]

    rel_dist_arr_list = []
    for k, ((name_i, trace_i), (name_j, trace_j)) in enumerate(pair_list):
        # Visualize desired relative distance and bounds
        dst_rel_pos = desired_trace[name_j] - desired_trace[name_i]
        dst_dist_arr = np.abs(dst_rel_pos)
        ax[k].plot(stamp_arr, dst_dist_arr, 'g--', linewidth=1)
    
        # Genrate true relative positions
        rel_x_arr = trace_j['X'] - trace_i['X']
        rel_y_arr = trace_j['Y'] - trace_i['Y']
        rel_dist_arr = np.sqrt(rel_x_arr**2 + rel_y_arr**2)
        rel_dist_arr_list.append(rel_dist_arr)

    iter_artists = []
    for k in range(len(pair_list)):
        plot, = ax[k].plot([], [], 'b', linewidth=1)
        iter_artists.append(plot)

    def animate(t):
        for k in range(len(pair_list)):
            rel_dist_arr = rel_dist_arr_list[k]
            iter_artists[k].set_data(stamp_arr[:t], rel_dist_arr[:t])
        return iter_artists

    ani = FuncAnimation(fig_dist, func=animate, frames=range(len(stamp_arr)),
        interval=10, blit=True, repeat=False)

    for k in range(len(pair_list)):
        ax[k].set_ylim(0.0, 12.0)
    plt.show()


def main(argv) -> None:
    trace_path = argv.trace_path
    settings_str = (trace_path / SETTINGS_JSON).read_text()

    airsim_settings = json.loads(settings_str)
    drone_settings = get_simple_flight_settings(airsim_settings['Vehicles'])
    veh_init_pose_dict = get_init_pose_dict(drone_settings)
    num_agents = len(veh_init_pose_dict)
    assert num_agents >= 2, "There must be at least two agents."
    name_list = sorted(veh_init_pose_dict.keys())
    print("Vehicle Names:", name_list)

    # Color mapping for each vehicle name
    vehicle_cmap = dict(zip(name_list,
                    plt.cm.rainbow(np.linspace(0, 1, num_agents))))

    form_seq = get_form_sequence(
        name_list, airsim_settings["MultirotorFormation"]["FormSequence"])

    vehicle_trace_dict, stamped_desired_forms = get_one_simulation_run(
        trace_path, veh_init_pose_dict, form_seq)

    orig_stamp_arr = next(iter(vehicle_trace_dict.values()))[T_KEY]
    # Create a "desired position trace" with matching timestamps
    desired_trace = align_with_airsim_rec(stamped_desired_forms, orig_stamp_arr)

    # Shift timestamps to start from 0
    desired_trace[T_KEY] -= orig_stamp_arr[0]
    stamp_arr = orig_stamp_arr - orig_stamp_arr[0]

    print(f"Animating the trace from `{trace_path}`")
    animate_one_trace_in_world_frame(
        stamp_arr, vehicle_trace_dict, desired_trace, vehicle_cmap)

    animate_one_trace_relative_dist(
        stamp_arr, vehicle_trace_dict, desired_trace
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'trace_path', type=Path,
        help="A folder containing one simulation trace. "
             "The folder should be created by AirSim. ")

    main(parser.parse_args())
