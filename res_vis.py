import matplotlib.pyplot as plt 
import pickle 
import numpy as np
from analyze_sim_traces import compute_step_bound,compute_dist_to_setpoint_bound_by_step_fun
MAX_REL_POS = 20.0  # meter


with open("data/datasets-AirsimNH-fog0000.pickle", "rb") as pkl_file:
    truth_error_pair = pickle.load(pkl_file)
    truth_error_list = [
        p for p in zip(*truth_error_pair) if p[0] <= MAX_REL_POS
    ]
truth_error_norm_pair_arr = np.array(truth_error_list)

pw_const_bound = compute_step_bound(
    truth_error_norm_pair_arr,
    x_cuts=np.linspace(0, MAX_REL_POS, 50),
    max_remove_ratio=0.10
)

fog_level_list = ['0000','0010','0020']
color_list = ['b','g','r']

fig_dist, ax = plt.subplots(
        num="True Relative Distance", nrows=3,
        sharex=True)

for i, fog_level in enumerate(fog_level_list):
    with open(f'res-AirsimNH-fog-{fog_level}.pickle', 'rb') as f:
        res = pickle.load(f)
        
    for key in res.keys():
        max_bound, min_bound, mean_bound = res[key]

        ax[key].fill_between(max_bound[:,0], max_bound[:,1], min_bound[:,1], color=color_list[i], alpha=0.5)
        ax[key].plot(*mean_bound.T, color=color_list[i], alpha=0.5)

        norm_dst_arr = np.full(max_bound[:,0].shape, 5)
        error_bound_arr = compute_dist_to_setpoint_bound_by_step_fun(
            3, norm_dst_arr, pw_const_bound)
        ax[key].plot(max_bound[:,0], 5+error_bound_arr, '-', c='r')
        ax[key].plot(max_bound[:,0], 5-error_bound_arr, '-', c='r')

        ax[key].plot(max_bound[:,0], 5+np.zeros(len(max_bound[:,0])), '--', c='g')
            
        # ax[key].plot()
        # ax[key].plot(*min_bound.T, c=color_list[i])
        # ax[key].plot(*mean_bound.T, c=color_list[i])

plt.show()