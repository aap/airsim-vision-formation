import abc

import cvxpy
import numpy as np


class VelocityControlBase(abc.ABC):
    def __init__(self, num_agents: int, adj_mat: np.ndarray,
                 max_vel: float, cmd_duration: float) -> None:
        assert adj_mat.shape == (num_agents, num_agents)
        assert not np.all(adj_mat.diagonal()), "No self loop edges"
        assert cmd_duration > 0.0
        assert max_vel > 0.0
        self._num_agents = num_agents
        self._adj_mat = adj_mat
        self._max_vel = max_vel
        self._cmd_duration = cmd_duration

    @property
    def cmd_duration(self) -> float:
        return self._cmd_duration

    @abc.abstractmethod
    def set_new_form(self, form: np.ndarray) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def compute_velocity_ctrl(self, rel_pos) -> np.ndarray:
        raise NotImplementedError


class PControl(VelocityControlBase):
    Kp = 1.0  # P control

    def __init__(self, num_agents: int, adj_mat: np.ndarray,
                 max_vel: float, cmd_duration: float) -> None:
        super().__init__(num_agents, adj_mat, max_vel, cmd_duration)
        self._tgt_rel_pos_mat = np.full(shape=(num_agents, num_agents), fill_value=np.nan + np.nan * 1j)

    def set_new_form(self, form: np.ndarray) -> None:
        self._tgt_rel_pos_mat = self._find_target_relative_positions(form)

    def _find_target_relative_positions(self, form: np.ndarray) -> np.ndarray:
        num_agents = self._num_agents
        adj_mat = self._adj_mat
        tgt_rel_pos = np.full(shape=(num_agents, num_agents), fill_value=np.nan + np.nan * 1j)
        for i in range(num_agents):
            agt_tgt_pos = form[i]
            nbr_iter = (j for j in range(num_agents) if adj_mat[i, j])
            for j in nbr_iter:
                nbr_tgt_pos = form[j]
                rel = nbr_tgt_pos - agt_tgt_pos
                tgt_rel_pos[i, j] = rel @ np.array([1., 1.j])
        return tgt_rel_pos

    def compute_velocity_ctrl(self, rel_pos) -> np.ndarray:
        num_agents = self._num_agents
        assert rel_pos.shape == (num_agents, num_agents)
        adj_mat = self._adj_mat
        tgt_rel_pos = self._tgt_rel_pos_mat
        err_mat = rel_pos - tgt_rel_pos

        ret_v2d = np.full(shape=(num_agents, 2), fill_value=np.nan)
        for i in range(num_agents):
            v_complex = self.Kp * sum(err_mat[i, j] for j in range(num_agents) if adj_mat[i, j])
            if abs(v_complex) > self._max_vel:  # Clip max velocity
                v_complex = (self._max_vel / abs(v_complex)) * v_complex
            ret_v2d[i] = (v_complex.real, v_complex.imag)
        return ret_v2d


class PControlDefaultStop(PControl):
    def __init__(self, num_agents: int, adj_mat: np.ndarray,
                 max_vel: float, cmd_duration: float) -> None:
        super().__init__(num_agents, adj_mat, max_vel, cmd_duration)

    def compute_velocity_ctrl(self, rel_pos: np.ndarray) -> np.ndarray:
        ret_v2d = super().compute_velocity_ctrl(rel_pos)
        fix_nan_ret_v2d = np.where(
            np.isnan(ret_v2d), 0.0, ret_v2d)
        return fix_nan_ret_v2d


class PControlSwitchTopology(PControl):
    def __init__(self, num_agents: int, adj_mat: np.ndarray,
                 max_vel: float, cmd_duration: float) -> None:
        super().__init__(num_agents, adj_mat, max_vel, cmd_duration)

    def compute_velocity_ctrl(self, rel_pos: np.ndarray) -> np.ndarray:
        orig_adj_mat = self._adj_mat
        # ignore nan by removing edges
        self._adj_mat = orig_adj_mat & ~np.isnan(rel_pos)
        ret_v2d = super().compute_velocity_ctrl(rel_pos)
        assert not np.isnan(ret_v2d).any()
        self._adj_mat = orig_adj_mat  # restore old graph
        return ret_v2d


class SDPControl(VelocityControlBase):
    def __init__(self, num_agents: int, adj_mat: np.ndarray,
                 max_vel: float, cmd_duration: float) -> None:
        super().__init__(num_agents, adj_mat, max_vel, cmd_duration)
        self._gain_mat = np.full(shape=(num_agents, num_agents), fill_value=np.nan + np.nan * 1j)

    def set_new_form(self, form: np.ndarray) -> None:
        self._gain_mat = self._find_gain_with_sdp(form)

    def _find_gain_with_sdp(self, form: np.ndarray) -> np.ndarray:
        """
        Fathian, K. et.al., "Robust Distributed Formation Control of Agents With Higher-Order Dynamics",
        IEEE CONTROL SYSTEMS LETTERS, VOL. 2, NO. 3, JULY 2018
        """
        num_agents = self._num_agents
        adj_mat = self._adj_mat
        formation = form

        z = formation @ np.array([1.0, 1.j])
        z_1_mat = np.column_stack((z, np.ones(num_agents)))
        u, _, _ = np.linalg.svd(z_1_mat)

        s_mat_bool = np.logical_not(adj_mat) ^ np.identity(num_agents, dtype=bool)
        s_mat = cvxpy.Constant(s_mat_bool.astype(float))
        q_mat = cvxpy.Constant(u[:, 2:])  # Take the last (num_agents-2) columns
        a_mat = cvxpy.Variable(shape=(num_agents, num_agents), hermitian=True)
        objective = cvxpy.Maximize(cvxpy.lambda_min(q_mat.H @ a_mat @ q_mat))
        constraints = [
            a_mat @ z_1_mat == 0,
            cvxpy.norm(a_mat) <= cvxpy.Constant(10.0),
            cvxpy.multiply(a_mat, s_mat) == 0
        ]
        cvx_prob = cvxpy.Problem(objective, constraints)
        cvx_prob.solve()

        gain_mat = -np.array(a_mat.value)
        return gain_mat

    def compute_velocity_ctrl(self, rel_pos) -> np.ndarray:
        num_agents = self._num_agents
        adj_mat = self._adj_mat
        gain_mat = self._gain_mat
        ret_v2d = np.full(shape=(num_agents, 2), fill_value=np.nan)
        for i in range(num_agents):
            v_complex = sum(gain_mat[i, j] * rel_pos[i, j]
                            for j in range(num_agents) if adj_mat[i, j])
            if abs(v_complex) > self._max_vel:  # Clip max velocity
                v_complex = (self._max_vel / abs(v_complex)) * v_complex
            ret_v2d[i] = (v_complex.real, v_complex.imag)
        return ret_v2d
