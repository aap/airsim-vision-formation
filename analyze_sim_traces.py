#!/usr/bin/env python3

import argparse
import csv
import json
from pathlib import Path
import pickle
import re
from typing import Mapping, Sequence, Tuple

from airsim import Pose
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import ConvexHull
from scipy.special import comb

from formation_config import get_form_sequence, get_init_pose_dict, get_simple_flight_settings

T_KEY = "TimeStamp"

SETTINGS_JSON = "settings.json"
AIRSIM_REC = "airsim_rec.txt"
SET_FORM_REC = "set_form_rec.txt"
TRUE_POSE_GLOB = "[0-9]*_true_pose.pickle"
TRUE_POSE_REGEX = fr"(?P<{T_KEY}>\d+)_true_pose.pickle"
PERC_COMPLEX_MAT_GLOB = "[0-9]*_perc_complex_mat.pickle"
PERC_COMPLEX_MAT_REGEX = fr"(?P<{T_KEY}>\d+)_perc_complex_mat.pickle"

PYPLOT_LINE_STYLES = [':', '--', '-', '-.']


def compute_level_bound(num_agents: int, error_bound: float) -> float:
    num_pairs: int = comb(num_agents, 2, exact=True)
    return (error_bound)**2 * (num_agents-1) * num_pairs


def compute_dist_to_setpoint_bound_by_const(
        num_agents: int, error_bound: np.ndarray) -> np.ndarray:
    num_pairs: int = comb(num_agents, 2, exact=True)
    return error_bound * np.sqrt(num_pairs)


def compute_dist_to_setpoint_bound_by_step_fun(
        num_agents: int, norm_dst_arr: np.ndarray, step_fun: np.ndarray
    ) -> np.ndarray:
    """
    Compute the bound on the distance from truth to setpoint, ǁz*-zǁ, using
    the following input
    + the norm of the setpoint ǁz*ǁ
    + the upper bounding function on perception error, gamma_max(ǁzǁ),
    + the upper bounding function on ǁz*-zǁ, const_bound(gamma_max)
      that takes in a perception error bound gamma_max.

    NOTE: This code assumes gamma_max is a non-decreasing step function.
    """
    assert len(step_fun.shape) == 2 and step_fun.shape[1] == 2, "Must be a kx2 array"
    num_pairs: int = comb(num_agents, 2, exact=True)
    c = np.sqrt(num_pairs)

    ret_arr = np.full(shape=norm_dst_arr.shape, fill_value=np.inf)
    for i in range(len(step_fun) - 1):
        x_b, y_b = step_fun[i+1][0], c*step_fun[i][1]
        is_bounded = (norm_dst_arr + y_b <= x_b)
        ret_arr = np.minimum(ret_arr, np.where(is_bounded, y_b, np.inf))
    return ret_arr


def compute_step_bound(
        xy_arr: np.ndarray,
        x_cuts: np.ndarray,
        max_remove_ratio: float = 0.0
    ) -> np.ndarray:
    """ Compute a piecewise constant upper bound function for given 2D data
        points.

        NOTE: We further requires the step function to be non-decreasing.

    This returns a 2D array [(x0, y0), ..., (xk, yk)] denoting the function
    below (left continuous to choose small value for cuts):
           | y0, if 0 <= x <= x0
    f(x) = | y0, if x0 < x <= x1
           | yi, if xi < x <= x{i+1}
           | inf, if xk < x
    """
    assert len(xy_arr.shape) == 2 and xy_arr.shape[1] == 2, "Must be a Nx2 array"
    assert np.all(xy_arr >= 0), "All data points should be nonnegative"
    assert np.all(np.isfinite(x_cuts)), "All cuts must be finite."
    assert np.all(x_cuts[:-1] < x_cuts[1:]), "Cuts must be strictly increasing."

    if x_cuts[0] != 0.0:
        x_cuts = np.insert(x_cuts, 0, 0.0)

    x_arr, y_arr = xy_arr.T
    bin_idx_arr = np.digitize(x_arr, x_cuts)

    # Create one more bin when (bin_idx-1) is -1
    y_bins = [[] for _ in range(len(x_cuts) + 1)]
    for x, y, bin_idx in zip(x_arr, y_arr, bin_idx_arr):
        y_bins[bin_idx-1].append(y)

    y_max_arr = np.zeros(shape=x_cuts.shape)
    for i in range(len(y_max_arr)):
        filter_idx = int(max_remove_ratio*len(y_bins[i]))
        if y_bins[i] and 0 <= filter_idx < len(y_bins[i]):
            y_bins[i].sort(reverse=True)
            ys = y_bins[i][filter_idx:]
            y_max_arr[i] = ys[0]

    # Make the function monotonic
    for i in range(len(y_max_arr) - 1):
        if y_max_arr[i] > y_max_arr[i+1]:
            y_max_arr[i+1] = y_max_arr[i]
    assert np.all(y_max_arr[:-1] <= y_max_arr[1:]), \
        "Step function must be non-decreasing."
    return np.column_stack((x_cuts, y_max_arr))


def create_step_func(pw_const_bound: np.ndarray):
    assert len(pw_const_bound.shape) == 2 \
        and pw_const_bound.shape[1] == 2, "Must be a Nx2 array"

    x_cuts, y_bounds = pw_const_bound.T

    def step_func(x_arr: np.ndarray):
        cond_list = [x_arr < x_cuts[0]]
        cond_list.extend(
            np.logical_and(x0 <= x_arr, x_arr < x1)
            for x0, x1 in zip(x_cuts[:-1], x_cuts[1:])
        )
        cond_list.append(x_cuts[-1] <= x_arr)
        const_list = [y_bounds[0]] + y_bounds.tolist()
        return np.piecewise(x_arr, cond_list, const_list)
    return step_func


def compute_pw_linear_bound(xy_arr: np.ndarray):
    """ Compute a piecewise continuous linear upper bound function for given
        2D data points.

    This returns a 2D array [(x0, y0), ..., (xk, yk)] with (x0, y0) = (0, 0)
    denoting the function below:
           | y0 + (x-x0)*(y1-y0)/(x1-x0), if 0 = x0 <= x < x1
    f(x) = | yi + (x-xi)*(y{i+1}-yi)/(x{i+1}-xi),  if xi <= x <= x{i+1}
           | yk,  if xk < x
    """
    assert len(xy_arr.shape) == 2 and xy_arr.shape[1] == 2, "Must be a Nx2 array."
    assert np.all(xy_arr >= 0), "All data points should be nonnegative."
    max_x, max_y = np.max(xy_arr, axis=0)

    # Add additional points to simplify right and bottom sides of convex hull.
    hull = ConvexHull(
        [(0., 0.), (max_x, 0.), (max_x, max_y)],
        incremental=True)
    hull.add_points(xy_arr)

    indices = hull.vertices.tolist()
    assert indices[:3] == [0, 1, 2]
    # In 2D, reverse vertices so it is clockwise.
    indices = indices[3:]
    indices.append(0)
    indices.reverse()
    indices.append(2)
    return hull.points[indices]


def get_stamped_pos(row) \
        -> Tuple[int, float, float, float]:
    return (
        int(row[T_KEY]),
        float(row['POS_X']),
        float(row['POS_Y']),
        float(row['POS_Z']),
    )


def align_with_airsim_rec(
        src_nanosec_data_arr: np.ndarray,
        dst_millisec_arr: np.ndarray
    ) -> np.ndarray:
    assert len(src_nanosec_data_arr) > 0

    ret_arr = np.empty(
        shape=dst_millisec_arr.shape[0],
        dtype=src_nanosec_data_arr.dtype
    )

    # Set to NaN before setting the first form
    data_len = len(ret_arr.dtype.names) - 1
    prev_row = np.array([(0,) + (np.nan + np.nan*1.j,)*data_len],
        dtype=src_nanosec_data_arr.dtype)
    prev_idx = 0
    for src_row in src_nanosec_data_arr:
        src_millisec = src_row[T_KEY] / 10**6

        curr_idx = np.searchsorted(dst_millisec_arr[0:], src_millisec)

        # Duplicate and copy the row
        ret_arr[prev_idx:curr_idx] = prev_row
        prev_idx = curr_idx
        prev_row = src_row

        if prev_idx >= len(dst_millisec_arr):
            break

    # Fill the rest after the last timestamp in source array
    if prev_idx < len(dst_millisec_arr):
        ret_arr[prev_idx:] = prev_row

    # Override all timestamps to align
    ret_arr[T_KEY] = dst_millisec_arr
    return ret_arr


def read_one_airsim_rec(airsim_rec_file, vehicle_dict: Mapping[str, Pose]):
    vehicle_trace_dict = {name: [] for name in vehicle_dict.keys()}

    rec_reader = csv.DictReader(airsim_rec_file, delimiter='\t')
    prev_stamp = -1
    stamp_count = 1
    skip_stamp_set = set()
    for row in rec_reader:
        stamped_pos = get_stamped_pos(row)
        vehicle_trace_dict[row['VehicleName']].append(stamped_pos)

        curr_stamp = stamped_pos[0]
        if curr_stamp == prev_stamp:
            stamp_count += 1  # inc counter
        else:
            if stamp_count != len(vehicle_dict):
                # Missing states for some vehicles in this timestamp
                skip_stamp_set.add(prev_stamp)
            stamp_count = 1  # reset counter
        prev_stamp = curr_stamp

    # Convert list to structured ndarray and filter skipped stamps
    # XXX interpolate states instead of removing timestamps?
    dtype = np.dtype([(T_KEY, int), ('X', float), ('Y', float), ('Z', float)])
    vehicle_trace_dict = {name: np.fromiter((s for s in trace if s[0] not in skip_stamp_set),
                                            dtype=dtype)
                          for name, trace in vehicle_trace_dict.items()}

    # Convert to the world frame from frames of initial positions
    for name in vehicle_dict.keys():
        trace = vehicle_trace_dict[name]
        trace['X'] += vehicle_dict[name].position.x_val
        trace['Y'] += vehicle_dict[name].position.y_val
        trace['Z'] += vehicle_dict[name].position.z_val

    stamp_arr = next(iter(vehicle_trace_dict.values()))[T_KEY]
    assert all(np.array_equal(trace[T_KEY], stamp_arr) for trace in vehicle_trace_dict.values()), \
        "Timestamps for all vehicles should be aligned."

    for trace_arr in vehicle_trace_dict.values():
        trace_arr.flags.writeable = False
    return vehicle_trace_dict


def read_one_set_form_rec(set_form_rec_file) -> Sequence[Tuple[int, int]]:
    rec_reader = csv.DictReader(set_form_rec_file, delimiter='\t')

    return [(int(row[T_KEY]), int(row['FormIndex']))
            for row in rec_reader]


def get_trace_path_list(parent_path: Path) -> Tuple[str, Sequence[Path]]:
    if not parent_path.is_dir():
        raise TypeError(f"{parent_path} is not a folder")

    # Match all folder names with datetime pattern
    # and filter those without a settings.json
    D = "[0-9]"
    FN_MATCH_DATETIME = "-".join([
        D*4, "[01]"+D, "[0-3]"+D, "[0-2]"+D, "[0-5]"+D, "[0-5]"+D
    ])
    trace_path_list = [p for p in parent_path.glob(FN_MATCH_DATETIME)
                       if (p / SETTINGS_JSON).exists()]
    if not trace_path_list:
        print("No trace folder is found.")
        return

    trace_path_list.sort()
    print("Checking if all settings.json are the same...", end="")
    trace_path_iter = iter(trace_path_list)
    trace_0_path = next(trace_path_iter)
    trace_0_setting_str = (trace_0_path / SETTINGS_JSON).read_text()

    filtered_trace_path_list = [trace_0_path]
    for trace_path in trace_path_iter:
        trace_setting_str = (trace_path / SETTINGS_JSON).read_text()
        if trace_setting_str == trace_0_setting_str:
            filtered_trace_path_list.append(trace_path)
            continue
        # else:
        raise UserWarning(
            f"settings.json in {trace_path} is not equal to the one in {trace_0_path}"
        )
    print("Done")
    print("List of folders to use:", [p.name for p in filtered_trace_path_list])
    return trace_0_setting_str, filtered_trace_path_list


def get_list_simulation_runs(
        trace_path_list: Sequence[Path],
        veh_init_pose_dict: Mapping[str, Pose],
        form_seq: Sequence[np.ndarray]
    ):
    list_sim_runs = []

    print("Reading files: ", end="")
    for trace_path in trace_path_list:
        print(trace_path.name, end=" ")
        vehicle_trace_dict, stamped_desired_pos_arr = get_one_simulation_run(
            trace_path, veh_init_pose_dict, form_seq)
        list_sim_runs.append((vehicle_trace_dict, stamped_desired_pos_arr))
    print("Done")
    return list_sim_runs


def get_one_simulation_run(
        trace_path: Path,
        veh_init_pose_dict: Mapping[str, Pose],
        form_seq: Sequence[np.ndarray]
    ):
    airsim_rec_path = trace_path / AIRSIM_REC
    set_form_rec_path = trace_path / SET_FORM_REC
    assert airsim_rec_path.exists()
    assert set_form_rec_path.exists()

    with open(airsim_rec_path, "r") as airsim_rec_file:
        vehicle_trace_dict = read_one_airsim_rec(airsim_rec_file, veh_init_pose_dict)
    with open(set_form_rec_path, "r") as set_form_rec_file:
        stamped_form_idx_list = read_one_set_form_rec(set_form_rec_file)
    field_names = [(T_KEY, int)] + \
        [(name, complex) for name in sorted(veh_init_pose_dict.keys())]
    dtype = np.dtype(field_names)
    stamped_desired_pos_arr = np.empty(
        shape=len(stamped_form_idx_list), dtype=dtype)
    for (stamp, form_idx), row in zip(stamped_form_idx_list, stamped_desired_pos_arr):
        row[T_KEY] = stamp
        assert len(field_names[1:]) == len(form_seq[form_idx])
        for (name, _), (x, y) in zip(field_names[1:], form_seq[form_idx]):
            row[name] = x + y*1.j
    stamped_desired_pos_arr.flags.writeable = False
    return vehicle_trace_dict, stamped_desired_pos_arr


def get_truth_complex_mat(tru_pose_arr: np.ndarray)\
        -> np.ndarray:
    num_agents = tru_pose_arr.shape[0]

    tru_pos2d_arr = tru_pose_arr[:, 0:2] @ np.array([1, 1j])
    tmp_arr = np.tile(tru_pos2d_arr, (num_agents, 1))
    true_rel_pos = tmp_arr - tmp_arr.T
    return true_rel_pos


def get_truth_error_norm_pairs(
        trace_path_list: Sequence[Path]
    ) -> Sequence[Tuple[float, float]]:
    truth_error_norm_pair_list: Sequence[Tuple[float, float]] = []

    for path in trace_path_list:
        true_pose_pkl_list = sorted(list(path.glob(TRUE_POSE_GLOB)))
        perc_complex_mat_pkl_list = sorted(list(path.glob(PERC_COMPLEX_MAT_GLOB)))

        assert len(true_pose_pkl_list) == len(perc_complex_mat_pkl_list)

        for true_pose_path, perc_mat_path in zip(true_pose_pkl_list, perc_complex_mat_pkl_list):
            m = re.match(TRUE_POSE_REGEX, true_pose_path.name)
            true_pose_stamp = int(m[T_KEY])

            m = re.match(PERC_COMPLEX_MAT_REGEX, perc_mat_path.name)
            perc_mat_stamp = int(m[T_KEY])
            assert true_pose_stamp == perc_mat_stamp

            with open(true_pose_path, 'rb') as true_pose_file:
                true_pose_arr = pickle.load(true_pose_file)
                true_pose_arr.flags.writeable = False
            with open(perc_mat_path, 'rb') as perc_mat_file:
                perc_mat = pickle.load(perc_mat_file)
                perc_mat.flags.writeable = False

            true_mat = get_truth_complex_mat(true_pose_arr)
            err_mat = perc_mat - true_mat

            norm_truth_arr = np.abs(true_mat).ravel()
            norm_error_arr = np.abs(err_mat).ravel()

            truth_error_norm_pair_list.extend(
                (t, e) for t, e in zip(norm_truth_arr, norm_error_arr)
                if np.isfinite(t) and np.isfinite(e)
            )
    return truth_error_norm_pair_list


def plot_traces_in_world_frame(list_sim_runs, vehicle_cmap):
    fig_pos = plt.figure("Position Trace")
    for vehicle_trace_dict, _ in list_sim_runs:
        # Plot XY positions in the world frame
        for name, trace in vehicle_trace_dict.items():
            fig_pos.gca().plot(trace['X'], trace['Y'], c=vehicle_cmap[name])
            # Mark end
            fig_pos.gca().scatter(trace['X'][-1:], trace['Y'][-1:], marker='x', c='r')
    fig_pos.gca().set_aspect("equal")


def plot_laplacian_potential(
        num_agents: int,
        list_sim_runs: Sequence[Mapping[str, Tuple[np.ndarray, np.ndarray]]],
        error_bounds: Sequence[float]
    ):
    fig_lya = plt.figure("Laplacian Potential")
    end_stamp = 0
    for vehicle_trace_dict, desired_pos_arr in list_sim_runs:
        stamp_arr = next(iter(vehicle_trace_dict.values()))[T_KEY]

        # Create a "desired position trace" with matching timestamps
        stamp_match_pos_arr = align_with_airsim_rec(desired_pos_arr, stamp_arr)

        # Shift timestamps to start from 0
        stamp_match_pos_arr[T_KEY] -= stamp_arr[0]
        aligned_stamp_arr = stamp_arr - stamp_arr[0]
        end_stamp = max(end_stamp, aligned_stamp_arr[-1])

        # Plot Laplacian potential function values
        lya_val_arr = np.zeros(shape=len(aligned_stamp_arr))

        nonrepeat_pair_iter = (
            (name_i, trace_i, name_j, trace_j)
            for name_i, trace_i in vehicle_trace_dict.items()
            for name_j, trace_j in vehicle_trace_dict.items()
            if name_i != name_j
        )
        for name_i, trace_i, name_j, trace_j in nonrepeat_pair_iter:
            dst_rel_pos = stamp_match_pos_arr[name_j] - stamp_match_pos_arr[name_i]
            dst_rel_x, dst_rel_y = np.real(dst_rel_pos), np.imag(dst_rel_pos)

            rel_x_arr = trace_j['X'] - trace_i['X']
            rel_y_arr = trace_j['Y'] - trace_i['Y']
            lya_val_arr += ((dst_rel_x - rel_x_arr)**2 + (dst_rel_y - rel_y_arr)**2)
        lya_val_arr = lya_val_arr / 2

        fig_lya.gca().plot(aligned_stamp_arr, lya_val_arr)

    for i, error_bound in enumerate(error_bounds):
        line_style = PYPLOT_LINE_STYLES[i % len(PYPLOT_LINE_STYLES)]
        LYA_LEVEL = compute_level_bound(num_agents, error_bound)
        fig_lya.gca().plot([0, end_stamp], [LYA_LEVEL, LYA_LEVEL], line_style, c='b')

    fig_lya.suptitle("Laplacian potential value w.r.t time", fontsize=20)
    fig_lya.gca().set_xlabel("time (ms)")


def plot_pairwise_relative_distance(
    num_agents: int, list_sim_runs: Sequence, pw_const_bound: np.ndarray):
    assert num_agents > 2, "Need more than two agents"
    # Plot upper bound line
    fig_dist, ax = plt.subplots(
        num="Relative Distance", nrows=comb(num_agents, 2, exact=True),
        sharex=True)

    START_STAMP = 0
    end_stamp = 0
    for vehicle_trace_dict, desired_pos_arr in list_sim_runs:
        stamp_arr = next(iter(vehicle_trace_dict.values()))[T_KEY]

        # Create a "desired position trace" with matching timestamps
        stamp_match_pos_arr = align_with_airsim_rec(desired_pos_arr, stamp_arr)

        # Shift timestamps to start from 0
        stamp_match_pos_arr[T_KEY] -= stamp_arr[0]
        aligned_stamp_arr = stamp_arr - stamp_arr[0]
        end_stamp = max(end_stamp, aligned_stamp_arr[-1])

        pair_iter = (((name_i, trace_i), (name_j, trace_j))
                     for name_i, trace_i in vehicle_trace_dict.items()
                     for name_j, trace_j in vehicle_trace_dict.items()
                     if name_i < name_j)
        for k, ((name_i, trace_i), (name_j, trace_j)) in enumerate(pair_iter):
            dst_rel_pos = stamp_match_pos_arr[name_j] - stamp_match_pos_arr[name_i]
            dst_rel_x, dst_rel_y = np.real(dst_rel_pos), np.imag(dst_rel_pos)

            rel_x_arr = trace_j['X'] - trace_i['X']
            rel_y_arr = trace_j['Y'] - trace_i['Y']

            # Visualize desired distance for each pair
            norm_dst_arr = np.sqrt(dst_rel_x**2 + dst_rel_y**2)
            ax[k].plot(aligned_stamp_arr, norm_dst_arr, '--', c='g')
            # Visualize upper bound on errors for each pair
            error_bound_arr = compute_dist_to_setpoint_bound_by_step_fun(
                num_agents, norm_dst_arr, pw_const_bound)

            upper_bound = norm_dst_arr + error_bound_arr
            lower_bound = norm_dst_arr - error_bound_arr
            ax[k].plot(aligned_stamp_arr, upper_bound , '-', c='red')
            ax[k].plot(aligned_stamp_arr, lower_bound, '-', c='red')

            # Visualize relative positions
            rel_dist_arr = np.sqrt(rel_x_arr**2 + rel_y_arr**2)
            ax[k].plot(aligned_stamp_arr, rel_dist_arr, c='b', linewidth=1)
            ax[k].set_ylim(0, 20)  # TODO select suitable limit


    fig_dist.suptitle("True relative positions w.r.t time "
                      "for each pair of drones",
                      fontsize=20)
    plt.setp(ax, xlim=(START_STAMP, end_stamp), ylim=(0.0, None), xlabel="time (ms)")


def main(argv):
    settings_str, trace_path_list = get_trace_path_list(argv.parent_path)

    airsim_settings = json.loads(settings_str)
    drone_settings = get_simple_flight_settings(airsim_settings['Vehicles'])
    veh_init_pose_dict = get_init_pose_dict(drone_settings)
    num_agents = len(veh_init_pose_dict)
    assert num_agents >= 2, "There must be at least two agents."
    name_list = sorted(veh_init_pose_dict.keys())
    print("Vehicle Names:", name_list)

    # Color mapping for each vehicle name
    vehicle_cmap = dict(zip(name_list,
                    plt.cm.rainbow(np.linspace(0, 1, num_agents))))

    MAX_REL_POS = 20.0  # meter

    form_seq = get_form_sequence(
        name_list, airsim_settings["MultirotorFormation"]["FormSequence"])

    list_sim_runs = get_list_simulation_runs(
        trace_path_list, veh_init_pose_dict, form_seq)

    with open("data/datasets-AirsimNH-fog0000.pickle", "rb") as pkl_file:

        truth_error_pair = pickle.load(pkl_file)
        truth_error_list = [
            p for p in zip(*truth_error_pair) if p[0] <= MAX_REL_POS
        ]
    truth_error_norm_pair_arr = np.array(truth_error_list)

    pw_const_bound = compute_step_bound(
        truth_error_norm_pair_arr,
        x_cuts=np.linspace(0, MAX_REL_POS, 50),
        max_remove_ratio=0.0
    )

    pw_linear_bound = compute_pw_linear_bound(
        truth_error_norm_pair_arr
    )
    fig_pairs = plt.figure("Norm of truth vs norm of error")
    fig_pairs.gca().set_aspect("equal")
    fig_pairs.gca().set_xlabel(r"ǁ ${q}_{ij}$ ǁ (m)", fontsize=14)
    fig_pairs.gca().set_ylabel(r"ǁ ${\hat{q}}_{ij} - {q}_{ij}$ ǁ (m)", fontsize=14)
    fig_pairs.gca().set_xlim(0, MAX_REL_POS)
    fig_pairs.gca().set_ylim(top=15)
    fig_pairs.gca().scatter(*truth_error_norm_pair_arr.T, s=2)
    fig_pairs.gca().step(*pw_const_bound.T, c='r', where='post')
    fig_pairs.gca().plot(*pw_linear_bound.T, c='b')

    plot_traces_in_world_frame(list_sim_runs, vehicle_cmap)

    plot_pairwise_relative_distance(num_agents, list_sim_runs, pw_const_bound)

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'parent_path', type=Path,
        help="Parent folder containing simulation traces. "
             "Data for each trace is stored in a folder created by AirSim. ")

    main(parser.parse_args())
