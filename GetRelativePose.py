import numpy as np
import cv2 
import json
# import matplotlib.pyplot as plt
from scipy.special import comb
from scipy.spatial.transform import Rotation as R
from scipy.linalg import svd
from multiprocessing import Pool

def coefsNumVer(mx1, mx2, my1, my2, nx2, ny2, r2, s1, s2):
    t2 = mx1*my2*r2 
    t3 = mx2*ny2*s1
    t4 = my1*nx2*s2
    t5 = mx1*nx2*s2*2.0
    t6 = my1*ny2*s2*2.0
    t7 = mx1*my2*nx2*2.0
    t8 = my2*r2*s1*2.0
    t9 = mx2*my1*r2
    t10 = mx1*ny2*s2
    t11 = mx2*my1*ny2*2.0
    t12 = mx2*r2*s1*2.0
    t13 = my2*nx2*s1
    coefsN = np.array([
        t2+t3+t4-mx2*my1*r2-mx1*ny2*s2-my2*nx2*s1,
        t11+t12-mx1*my2*ny2*2.0-mx1*r2*s2*2.0,
        t7+t8-mx2*my1*nx2*2.0-my1*r2*s2*2.0,
        t5+t6-mx2*nx2*s1*2.0-my2*ny2*s1*2.0,
        -t2-t3+t4+t9+t10-my2*nx2*s1,
        -t5+t6+mx2*nx2*s1*2.0-my2*ny2*s1*2.0,
        t7-t8-mx2*my1*nx2*2.0+my1*r2*s2*2.0,
        -t2+t3-t4+t9-t10+t13,
        -t11+t12+mx1*my2*ny2*2.0-mx1*r2*s2*2.0,
        t2-t3-t4-t9+t10+t13
    ])
    return coefsN

def coefsDenVer(mx2, my2, nx1, nx2, ny1, ny2, r1, r2, s2):
    t2 = mx2*ny1*r2
    t3 = my2*nx2*r1
    t4 = nx1*ny2*s2
    t5 = mx2*nx2*r1*2.0
    t6 = my2*ny2*r1*2.0
    t7 = mx2*nx2*ny1*2.0
    t8 = ny1*r2*s2*2.0
    t9 = my2*nx1*r2
    t10 = nx2*ny1*s2
    t11 = my2*nx1*ny2*2.0
    t12 = nx1*r2*s2*2.0
    t13 = mx2*ny2*r1
    coefsD = np.array([
        t2+t3+t4-mx2*ny2*r1-my2*nx1*r2-nx2*ny1*s2,
        t11+t12-my2*nx2*ny1*2.0-nx2*r1*s2*2.0,
        t7+t8-mx2*nx1*ny2*2.0-ny2*r1*s2*2.0,
        t5+t6-mx2*nx1*r2*2.0-my2*ny1*r2*2.0,
        t2-t3-t4+t9+t10-mx2*ny2*r1,
        t5-t6-mx2*nx1*r2*2.0+my2*ny1*r2*2.0,
        -t7+t8+mx2*nx1*ny2*2.0-ny2*r1*s2*2.0,
        -t2+t3-t4-t9+t10+t13,
        t11-t12-my2*nx2*ny1*2.0+nx2*r1*s2*2.0,
        -t2-t3+t4+t9-t10+t13
    ])
    return coefsD

def coefsNumDen(
        a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, 
        b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, 
    ):
    coefsND = np.array([
        a1*b1,
        a1*b2+a2*b1,
        a2*b2+a1*b5+a5*b1,
        a2*b5+a5*b2,
        a5*b5,
        a1*b3+a3*b1,
        a2*b3+a3*b2+a1*b6+a6*b1,
        a2*b6+a3*b5+a5*b3+a6*b2,
        a5*b6+a6*b5,
        a3*b3+a1*b8+a8*b1,
        a3*b6+a6*b3+a2*b8+a8*b2,
        a6*b6+a5*b8+a8*b5,
        a3*b8+a8*b3,
        a6*b8+a8*b6,
        a8*b8,
        a1*b4+a4*b1,
        a2*b4+a4*b2+a1*b7+a7*b1,
        a2*b7+a4*b5+a5*b4+a7*b2,
        a5*b7+a7*b5,
        a3*b4+a4*b3+a1*b9+a9*b1,
        a3*b7+a4*b6+a6*b4+a7*b3+a2*b9+a9*b2,
        a6*b7+a7*b6+a5*b9+a9*b5,
        a3*b9+a4*b8+a8*b4+a9*b3,
        a6*b9+a7*b8+a8*b7+a9*b6,
        a8*b9+a9*b8,
        a4*b4+a1*b10+a10*b1,
        a4*b7+a7*b4+a2*b10+a10*b2,
        a7*b7+a5*b10+a10*b5,
        a3*b10+a4*b9+a9*b4+a10*b3,
        a6*b10+a7*b9+a9*b7+a10*b6,
        a8*b10+a9*b9+a10*b8,
        a4*b10+a10*b4,
        a7*b10+a10*b7,
        a9*b10+a10*b9,
        a10*b10
    ])
    return coefsND

def Coefs(m1, m2) -> np.ndarray:
    numPts = m1.shape[1]

    idxBin1 = np.zeros((2,int(comb(numPts,2))-1))

    counter = 0
    for i in range(numPts-2):
        for j in range(i+1, numPts):
            idxBin1[:,counter] = np.array([int(i),int(j)])
            counter = counter+1
    idxBin1 = idxBin1.astype(int)
    mx1 = m1[0,idxBin1[0,:]]
    my1 = m1[1,idxBin1[0,:]]
    nx1 = m2[0,idxBin1[0,:]]
    ny1 = m2[1,idxBin1[0,:]]

    mx2 = m1[0,idxBin1[1,:]]
    my2 = m1[1,idxBin1[1,:]]
    nx2 = m2[0,idxBin1[1,:]]
    ny2 = m2[1,idxBin1[1,:]]

    s1 = m1[2,idxBin1[0,:]]
    r1 = m2[2,idxBin1[0,:]]
    s2 = m1[2,idxBin1[1,:]]
    r2 = m2[2,idxBin1[1,:]]

    coefsN = coefsNumVer(mx1, mx2, my1, my2, nx2, ny2, r2, s1, s2).T
    coefsD = coefsDenVer(mx2, my2, nx1, nx2, ny1, ny2, r1, r2, s2).T

    numEq = int(comb(numPts,3))
    idxBin2 = np.zeros((2, numEq))
    counter = 0
    counter2 = 0
    for i in range(numPts-1,1,-1):
        for j in range(1+counter2, i+counter2):
            for k in range(j+1,i+counter2+1):
                idxBin2[:,counter] = np.array([j-1,k-1])
                counter = counter + 1
        counter2 = i+counter2
    # print(counter, counter2)
    idxBin2 = idxBin2.astype(int)
    a1 = np.concatenate((coefsN[idxBin2[0,:],0],coefsD[idxBin2[0,:],0]))
    a2 = np.concatenate((coefsN[idxBin2[0,:],1],coefsD[idxBin2[0,:],1]))
    a3 = np.concatenate((coefsN[idxBin2[0,:],2],coefsD[idxBin2[0,:],2]))
    a4 = np.concatenate((coefsN[idxBin2[0,:],3],coefsD[idxBin2[0,:],3]))
    a5 = np.concatenate((coefsN[idxBin2[0,:],4],coefsD[idxBin2[0,:],4]))
    a6 = np.concatenate((coefsN[idxBin2[0,:],5],coefsD[idxBin2[0,:],5]))
    a7 = np.concatenate((coefsN[idxBin2[0,:],6],coefsD[idxBin2[0,:],6]))
    a8 = np.concatenate((coefsN[idxBin2[0,:],7],coefsD[idxBin2[0,:],7]))
    a9 = np.concatenate((coefsN[idxBin2[0,:],8],coefsD[idxBin2[0,:],8]))
    a10 = np.concatenate((coefsN[idxBin2[0,:],9],coefsD[idxBin2[0,:],9]))

    b1 = np.concatenate((coefsD[idxBin2[1,:],0],coefsN[idxBin2[1,:],0]))
    b2 = np.concatenate((coefsD[idxBin2[1,:],1],coefsN[idxBin2[1,:],1]))
    b3 = np.concatenate((coefsD[idxBin2[1,:],2],coefsN[idxBin2[1,:],2]))
    b4 = np.concatenate((coefsD[idxBin2[1,:],3],coefsN[idxBin2[1,:],3]))
    b5 = np.concatenate((coefsD[idxBin2[1,:],4],coefsN[idxBin2[1,:],4]))
    b6 = np.concatenate((coefsD[idxBin2[1,:],5],coefsN[idxBin2[1,:],5]))
    b7 = np.concatenate((coefsD[idxBin2[1,:],6],coefsN[idxBin2[1,:],6]))
    b8 = np.concatenate((coefsD[idxBin2[1,:],7],coefsN[idxBin2[1,:],7]))
    b9 = np.concatenate((coefsD[idxBin2[1,:],8],coefsN[idxBin2[1,:],8]))
    b10 = np.concatenate((coefsD[idxBin2[1,:],9],coefsN[idxBin2[1,:],9]))

    coefsND = coefsNumDen(
        a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, 
        b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, 
    ).T

    C = coefsND[0:numEq,:] - coefsND[numEq:2*numEq,:]
    return C


def QuEst(m, n):
    """
        Implements QuEst_5Pt_Ver7_8 in ./QuEst/QuEst_5Pt_Ver7_8.m
    """

    # Preallocate variables
    Idx = [ 
        [1,   2,   5,  11,  21,   3,   6,  12,  22,   8,  14,  24,  17,  27,  31,   4,   7,  13,  23,   9,  15,  25,  18,  28,  32,  10,  16,  26,  19,  29,  33,  20,  30,  34,  35],
        [2,     5,    11,    21,    36,     6,    12,    22,    37,    14,    24,    39,    27,    42,    46,     7,    13,    23,    38,    15,    25,    40,    28,    43,    47,    16,    26,    41,    29,    44,    48,    30,    45,    49,    50],
        [3,     6,    12,    22,    37,     8,    14,    24,    39,    17,    27,    42,    31,    46,    51,     9,    15,    25,    40,    18,    28,    43,    32,    47,    52,    19,    29,    44,    33,    48,    53,    34,    49,    54,    55],
        [4,     7,    13,    23,    38,     9,    15,    25,    40,    18,    28,    43,    32,    47,    52,    10,    16,    26,    41,    19,    29,    44,    33,    48,    53,    20,    30,    45,    34,    49,    54,    35,    50,    55,    56]
    ]
    Idx = np.array(Idx)
    Idx = Idx-1


    Cf = Coefs(m,n)

    numEq = Cf.shape[0]

    A = np.zeros((4*numEq,56))
    for i in range(4):
        idx = Idx[i,:]
        A[i*numEq:(i+1)*numEq,idx] = Cf
    
    u,s,vh = svd(A)
    vh = vh.T
    N = vh[:,36:56]

    idx = Idx[0,:]
    A0 = N[idx,:]
    idx = Idx[1,:]
    A1 = N[idx,:]
    idx = Idx[2,:]
    A2 = N[idx,:]
    idx = Idx[3,:]
    A3 = N[idx,:]

    tmp = np.concatenate((A1,A2,A3),axis=1)
    B,_,_,_ = np.linalg.lstsq(A0,tmp)

    B1 = B[:,0:20]
    B2 = B[:,20:40]
    B3 = B[:,40:60]
    
    # Find eigen vectors
    # Initial guess for the common eigenvectors
    _,V1 = np.linalg.eig(B1)
    _,V2 = np.linalg.eig(B2)
    _,V3 = np.linalg.eig(B3)

    Ve = np.concatenate((V1,V2,V3),axis=1)

    Vy = np.imag(Ve)
    imagIdxBool = np.sum(np.abs(Vy),axis=0)>10*np.finfo(float).eps
    Viall = Ve[:,imagIdxBool]
    srtIdx = np.argsort(np.real(Viall[0,:]))
    Vi = Viall[:,srtIdx[::2]]
    Vr = Ve[:,~imagIdxBool]
    tmp = np.concatenate((Vi,Vr),axis=1)
    V0 = np.real(tmp)

    X5 = N@V0 

    sign_array = np.sign(X5[0,:])
    for i in range(X5.shape[0]):
        X5[i,:] = X5[i,:]*sign_array
    w = X5[0,:]**(1/5)
    w4 = w**4
    x = X5[1,:]/w4 
    y = X5[2,:]/w4 
    z = X5[3,:]/w4
    w = np.expand_dims(w,axis=0)
    x = np.expand_dims(x,axis=0)
    y = np.expand_dims(y,axis=0)
    z = np.expand_dims(z,axis=0)

    Q = np.concatenate((w,x,y,z),axis=0)

    QNrm = np.linalg.norm(Q,axis=0)
    for i in range(Q.shape[0]):
        Q[i,:] = Q[i,:]/QNrm

    return Q

def QuatResidue(m1, m2, qSol):
    C0 = Coefs(m1, m2)
    q1 = qSol[0,:]
    q2 = qSol[1,:]
    q3 = qSol[2,:]
    q4 = qSol[3,:]

    xVec = [
        q1**4,
        q1**3*q2,
        q1**2*q2**2,
        q1*q2**3,
        q2**4,
        q1**3*q3,
        q1**2*q2*q3,
        q1*q2**2*q3,
        q2**3*q3,
        q1**2*q3**2,
        q1*q2*q3**2,
        q2**2*q3**2,
        q1*q3**3,
        q2*q3**3,
        q3**4,
        q1**3*q4,
        q1**2*q2*q4,
        q1*q2**2*q4,
        q2**3*q4,
        q1**2*q3*q4,
        q1*q2*q3*q4,
        q2**2*q3*q4,
        q1*q3**2*q4,
        q2*q3**2*q4,
        q3**3*q4,
        q1**2*q4**2,
        q1*q2*q4**2,
        q2**2*q4**2,
        q1*q3*q4**2,
        q2*q3*q4**2,
        q3**2*q4**2,
        q1*q4**3,
        q2*q4**3,
        q3*q4**3,
        q4**4
    ]

    residuMat = C0@xVec 

    residu = np.sum(np.abs(residuMat), axis = 0)
    return residu

def FindTransDepth(m, n, Q):
    if Q.shape[0] != 3:
        w,x,y,z = Q
        r = R.from_quat([x,y,z,w])
        Rot = r.as_matrix()
    else:
        Rot = Q 
    I = np.eye(3)
    numPts = m.shape[1]
    numInp = 1
    T = np.zeros((3,numInp))
    Z1 = np.zeros((numPts,numInp))
    Z2 = np.zeros((numPts,numInp))
    Res = np.zeros((1,numInp))
    for k in range(numInp):
        C = np.zeros((3*numPts, 2*numPts+3))
        for i in range(numPts):
            C[(i*3):(i+1)*3,0:3] = I 
            Rmi = r.apply(m[:,i])
            Rmi = np.expand_dims(Rmi,axis=1)
            ni = np.expand_dims(n[:,i],axis=1)
            tmp = np.concatenate((Rmi,-ni), axis=1)
            C[(i*3):(i+1)*3,i*2+3:i*2+5] = tmp

        _, S, N = svd(C)
        N = N.T 
        Y = N[:,-1]

        t = Y[0:3]
        z = Y[3:]

        numPos = np.sum(z>0)
        numNeg = np.sum(z<0)
        if numPos < numNeg:
            t = -t 
            z = -z 
        z1 = z[0::2]
        z2 = z[1::2]
        
        T[:,k] = t 
        Z1[:,k] = z1 
        Z2[:,k] = z2 
        Res[:,k] = S[-1]

    return T, Z1, Z2, Rot, Res

def ParallelForLoops(i, n, flag, adj, mchs, tmp_pts, K, maxZPts):
    pts = []
    for kps in tmp_pts:
        tmp_kp = []
        for point in kps:
            kp = cv2.KeyPoint(x=point[0][0],y=point[0][1],size=point[1],angle=point[2],response=point[3],octave=point[4],class_id=point[5])
            tmp_kp.append(kp)
        pts.append(tmp_kp)

    Q = np.zeros((4,n))
    T = np.zeros((3,n))
    for j in range(n):
        if adj[i,j]:
            pi = []
            pj = []
            for k in range(mchs[i][j].shape[0]):
                pi.append(list(pts[i][mchs[i][j][k,0]].pt)+[1])
                pj.append(list(pts[j][mchs[i][j][k,1]].pt)+[1])

            # Euclidean coordinates
            numpts = len(pi)
            mi,_,_,_ = np.linalg.lstsq(K,np.array(pi).T)
            mj,_,_,_ = np.linalg.lstsq(K,np.array(pj).T)
            
            # Recover relative roation using QuEst RANSAC
            M = QuEst(mi, mj)

            res = QuatResidue(mi, mj, M)
            # [resMin,mIdx] = min(abs(res))
            # q = Q(:,mIdx)
            mIdx = np.argmin(np.abs(res))
            q = M[:,mIdx]
            Q[:,j] = q
            
            numPts = min(maxZPts, mi.shape[1])
            tij, z1, z2, R, Res = FindTransDepth(
                mj[:,0:numPts],mi[:,0:numPts], q
            )

            s = np.mean(np.abs(z1))
            tij = tij.flatten()
            T[:,j] = tij/s
    
    # if flag[i] == 0:
    #     continue 
    # Qall[:,:,i] = Q 
    # Tall[:,:,i] = T
    return Q, T

def GetRelativePose(imgArrayInp, imgWidthInp, imgHeightInp, saveIdx, adj, itr):
    pool = Pool()
    
    # Transform image to vector
    imgArray = imgArrayInp 

    imgWidth = imgWidthInp 
    imgHeight = imgHeightInp 

    # Number of pixels in each image
    arrayLength = imgWidth*imgHeight

    # Number of agents
    n = adj.shape[0]

    # Flag to show if pose estimation was successful
    flag = np.ones(n)

    # # Reshape images into a matrix
    # imgs = np.zeros((imgHeight,imgWidth,3,n),np.uint8)
    # for i in range(n):
    #     pass

    # Turn RGB to gray
    gray_scale_images = []
    for i in range(n):
        image = imgArray[i]
        gray_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray_scale_images.append(gray_img)

    # Camera calibration matrix
    x0 = imgWidth/2 
    y0 = imgHeight/2
    #  focal length f = fx = fy, f can be derived from the field of view angle FoV:
    # FoV = 2 * arctan( imgWidth / 2f) <=> f = imgWidth / (2*tan(FoV/2))
    # By default in AirSim, FoV = 90° => f = imgWidth / (2*tan(45°) = imgWidth / 2
    fx = x0 
    fy = fx
    K = np.array([[fx, 0, x0],
                  [0, fy, y0],
                  [0, 0, 1]])

    # Extract feature points from all images
    surfThresh = 200
    minPts = 5
    maxPts = float('inf')
    fdp = []    # Feature point descriptors
    pts = []    # Feature point pixel coordinates
    for i in range(n):
        img = gray_scale_images[i]
        orb = cv2.ORB_create()
        kp, des = orb.detectAndCompute(img,None)
        numPts = min(maxPts, len(kp))
        fdp.append(des[:numPts])
        pts.append(kp[:numPts])

    # Match feature points of UAVs that are neighbors
    mchs = [[[] for i in range(n)] for i in range(n)]
    mchs_dmatch = [[[] for i in range(n)] for i in range(n)]
    for i in range(n-1):
        for j in range(i+1,n):
            if adj[i,j]>0:
                bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
                matches = bf.match(fdp[i],fdp[j])
                matches = sorted(matches, key = lambda x:x.distance)
                matches = matches[:50]
                # img3 = cv2.drawMatches(
                #     gray_scale_images[i],
                #     pts[i],
                #     gray_scale_images[j],
                #     pts[j],
                #     matches[:20],
                #     None,
                #     flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS
                # )
                # plt.imshow(img3),plt.show()
                mchs_dmatch[i][j] = matches 
                index_pairs = []
                for match in matches:
                    index_pairs.append([match.queryIdx, match.trainIdx])
                mchs[i][j] = np.array(index_pairs)
                mchs[j][i] = np.flip(mchs[i][j], axis = 1)
   
    # Use only common matched feature points
    numMch = np.zeros(n)
    for i in range(n):
        mchi = None 
        for j in range(n):
            if adj[i,j]:
                if mchi is None:
                    mchi = mchs[i][j][:,0]
                else:
                    mchi = np.intersect1d(mchi, mchs[i][j][:,0])
        
        # UPdate matched points to only include common points
        # idxi = []
        for j in range(n):
            if adj[i,j]:
                idxi = np.in1d(mchi,mchs[i][j][:,0]).nonzero()[0]
                mchs[i][j] = mchs[i][j][idxi,:]
        numMch[i] = idxi.shape[0]

    for i in range(n):
        if numMch[i] < minPts:
            flag[i] = 0

    maxZPts = 20
    ranThresh = 2e-6
    Qall = np.ones((4,n,n))
    Tall = np.zeros((3,n,n))

    job_dict = {}
    for i in range(n):
        if flag[i] == 1:
            # tmp_pts = []
            # for kps in pts:
            #     tmp_kp = []
            #     for kp in kps:
            #         tmp_kp.append((kp.pt, kp.size, kp.angle, kp.response, kp.octave, kp.class_id))
            #     tmp_pts.append(tmp_kp)
            # job = pool.apply_async(func=ParallelForLoops, args=(i, n, flag, adj, mchs, tmp_pts, K, maxZPts))   
            # job_dict[i] = job
            Q = np.zeros((4,n))
            T = np.zeros((3,n))
            for j in range(n):
                if adj[i,j]:
                    pi = []
                    pj = []
                    for k in range(mchs[i][j].shape[0]):
                        pi.append(list(pts[i][mchs[i][j][k,0]].pt)+[1])
                        pj.append(list(pts[j][mchs[i][j][k,1]].pt)+[1])

                    # Euclidean coordinates
                    numpts = len(pi)
                    mi,_,_,_ = np.linalg.lstsq(K,np.array(pi).T)
                    mj,_,_,_ = np.linalg.lstsq(K,np.array(pj).T)
                    
                    # Recover relative roation using QuEst RANSAC
                    M = QuEst(mi, mj)

                    res = QuatResidue(mi[:,:5], mj[:,:5], M)
                    # [resMin,mIdx] = min(abs(res))
                    # q = Q(:,mIdx)
                    mIdx = np.argmin(np.abs(res))
                    q = M[:,mIdx]
                    if np.isnan(q[0]):
                        continue
                    Q[:,j] = q
                    
                    numPts = min(maxZPts, mi.shape[1])
                    tij, z1, z2, R, Res = FindTransDepth(
                        mj[:,0:numPts],mi[:,0:numPts], q
                    )

                    s = np.mean(np.abs(z1))
                    tij = tij.flatten()
                    T[:,j] = tij/s
            
            if flag[i] == 0:
                continue 
            Qall[:,:,i] = Q 
            Tall[:,:,i] = T 
    # for job_idx in job_dict:
    #     Q, T = job_dict[job_idx].get(timeout=100000)
    #     Qall[:,:,job_idx] = Q 
    #     Tall[:,:,job_idx] = T
    
    Q = np.zeros((n**2, 4))
    T = np.zeros((n**2, 3))
    for i in range(n):
        for j in range(n):
            if i!=j:
                idx = i*n+j 
                Q[idx,:] = Qall[:,j,i]
                T[idx,:] = Tall[:,j,i]
    return Q, T

if __name__ == "__main__":
    with open('./datasets/data3/agent_states.json','r') as f:
        agent_state_dict = json.load(f)
    

    # imgArrayInp, imgWidthInp, imgHeightInp, saveIdx, adj, itr
    num_agents = len(agent_state_dict)
    imgArrayInp = []
    imgWidthInp = 0
    imgHeightInp = 0
    adj = np.ones((num_agents,num_agents)) - np.eye(num_agents)
    for agent_name in agent_state_dict:
        image = cv2.imread(f'./datasets/data3/{agent_name}.png')
        imgArrayInp.append(image)
        imgWidthInp = image.shape[1]
        imgHeightInp = image.shape[0]

    saveIdx = 1
    iter = 0
    Q, T = GetRelativePose(imgArrayInp,imgWidthInp,imgHeightInp,saveIdx,adj,iter)
    Tw = np.array([T[:,1], T[:,0], T[:,2]]).T
    # Compute ground truth
    agent_pos = []
    agent_ori = []
    num_agent = 0
    for agent in agent_state_dict:
        agent_pos.append([
            agent_state_dict[agent]['position']['x'],
            agent_state_dict[agent]['position']['y'],
            agent_state_dict[agent]['position']['z'],
        ])
        agent_ori.append([
            agent_state_dict[agent]['orientation']['x'],    
            agent_state_dict[agent]['orientation']['y'],    
            agent_state_dict[agent]['orientation']['z'],    
            agent_state_dict[agent]['orientation']['w'],    
        ])
        num_agent += 1

    res = []
    for i in range(num_agent):
        for j in range(num_agent):
            if i!= j:
                orientation = agent_ori[i]
                r = R.from_quat(orientation)
                r = r.inv()
                # roll, pitch, yaw = r.as_euler('xyz')
                # r_mat = r.as_matrix()

                pos = np.array(agent_pos[i])
                pos1 = np.array(agent_pos[j])

                pos2 = -pos+r.apply(pos1)
                res.append(pos2)
            if i == j:
                res.append(np.array([0,0,0]))
    print(res)